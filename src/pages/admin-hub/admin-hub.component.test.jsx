import React from 'react';
import { shallow } from 'enzyme';

import AdminHub from './admin-hub.component';



//test to see that all the elements are bing rendered
describe('create-category component', () => {
  
  // create a new component
  const createComp = () =>
    shallow(
      <AdminHub/>
    );

    //test to see if components exist
    it('renders login', () => {
      const component = createComp();

      expect(component.find("div")      .length).toBe(1);
      expect(component.find("Link")     .length).toBe(4);
      expect(component.find("h1")       .length).toBe(1);
    });
        
});