import React from 'react'
import { useSelector } from 'react-redux';
import {selectAuthenticationService } from '../../redux/service/service.selectors';

import { Navigate } from 'react-router-dom';

import AdminHub from './admin-hub.component';

export default () => {

    // ------------------------------------------- state -------------------------------------------

    const authenticationService   = useSelector(selectAuthenticationService);

    return !authenticationService.isAdmin() ?
    (<Navigate to='/subredditals'/>) : 
    (<AdminHub />)
}
