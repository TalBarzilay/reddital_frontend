import React from 'react'
import { Link } from 'react-router-dom';
import styles from './admin-hub.module.css';

export default () => {
    return (
        <div className={`${styles.container}`}>

            <h1 className={`${styles.title} lobster-font`}>Admin Hub</ h1>
            <Link className= {`${styles.headerlink} playfair-font`} to="/createCategory">create category</Link>
            <Link className= {`${styles.headerlink} playfair-font`} to="/createSubreddital">create subreddital</Link>
            <Link className= {`${styles.headerlink} playfair-font`} to="/blockUser">Ban User</Link>
            <Link className= {`${styles.headerlink} playfair-font`} to="/bi">Bi</Link>
        </div>
    )
}
