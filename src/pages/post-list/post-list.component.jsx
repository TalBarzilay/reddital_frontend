import React from 'react'
import PostPriview from '../../components/post-preview';
import styles from './post-list.module.css';

export default function PostList({posts, createPost, onSubDelete, isAdmin}) {


    return (
        <div className={`${styles.postsContainer}`} >

            <div className={`${styles.buttonsContainer}`}>
                <button className={`buton ${styles.createButton} sil-font`} onClick={createPost} >Create a Post</button>
            
                {isAdmin? 
                    <button className={`buton ${styles.deleteButton} sil-font`} onClick={onSubDelete} >Delete Subreddital</button>
                    : null
                }
            </div>


            {posts.map(post => ((post.blocked && isAdmin) || !post.blocked) ? 
                <PostPriview key={post.id} {...post} /> :
                null
            )}
        </div>
    )
} 
