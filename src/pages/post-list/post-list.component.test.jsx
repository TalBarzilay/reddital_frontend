import React from 'react';
import { shallow } from 'enzyme';

import PostList from './post-list.component'

describe('post-list component', () => {

    const createComponent = () =>
    shallow(
      <PostList
        posts = {[]}  
        createPost= {() => {}}

        isAdmin={false}
      />
    );

    // test that reducer returns currect inital state without arguments
    it('render post-list', () => {
        const postlist = createComponent();

        expect(postlist.find("div")   .length).toBe(2);
        expect(postlist.find("button").length).toBe(1);
    });

});