import React, { useEffect, useCallback } from 'react';

import PostList from './post-list.component';

import { useDispatch, useSelector } from 'react-redux';
import { INIT_POSTS } from '../../redux/posts-of-sub/posts-of-sub.actions';
import { selectPosts } from '../../redux/posts-of-sub/posts-of-sub.selectors';
import { selectMessageService, selectPostService, selectCategoryService, selectAuthenticationService } from '../../redux/service/service.selectors';

import { useMatch, useNavigate } from 'react-router-dom';

const PostListIndex = () => {

    // ------------------------------------------- state -------------------------------------------
  
    const match = useMatch('/:subreddital/posts');
    const navigate = useNavigate();

    const posts           = useSelector(selectPosts);
    const messageService  = useSelector(selectMessageService);
    const postService     = useSelector(selectPostService);
    const categoryService = useSelector(selectCategoryService);
    const authService     = useSelector(selectAuthenticationService);

    const dispatch = useDispatch();


    // ------------------------------------------- functions -------------------------------------------

     const initState = useCallback(async () => {
        try{
            const response = await postService.posts(match.params.subreddital);
            dispatch(INIT_POSTS(response.data));
        } catch(err) {
            messageService.err(err);
        }
    }, [postService, messageService,dispatch, match.params.subreddital]);

    const deleteSub = useCallback(async () => {
        try{
            await categoryService.deleteSub(match.params.subreddital);
            messageService.success('subreddital deleted succesfully');

            navigate(`/subredditals`)
        } catch(err){
            messageService.err(err);
        }
    }, [navigate, categoryService, messageService, match.params.subreddital]);

    // ------------------------------------------- init with posts -------------------------------------------
    
    useEffect(() => {
        initState();
    }, [initState]);

    useEffect(() => {
        messageService.info('Did you know?', 'Hover on the post to see its content!');
    }, [messageService]);
    
    // ------------------------------------------- return statement -------------------------------------------

    return (
        <PostList 
            createPost = {() => navigate(`/${match.params.subreddital}/post`)}
            
            onSubDelete={deleteSub} 
            isAdmin = {authService.isAdmin()}
            
            posts = {posts}
        />
    )
};





export default PostListIndex;
