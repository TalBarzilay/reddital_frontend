import React from 'react';
import { shallow, mount } from 'enzyme';

import {Provider} from 'react-redux';
import store from '../../redux/store';  

import PostList from './index';


jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router'), // use actual for all non-hook parts
    useMatch: () => ({params:{reddital:'askTal'}}),
    useNavigate: () => ({}),
}));


describe('post-list index', () => {

    // create a new component
    const createIndex = () =>
        shallow(<Provider store = {store} ><PostList/></Provider>);

    
    beforeEach(() => {
        jest.spyOn(React, "useEffect").mockImplementationOnce(cb => cb());     
    });

    // test that reducer returns currect inital state without arguments
    it('test index', () => {
        const wrapper = createIndex();

        expect(wrapper.length).toBe(1)
    });

});