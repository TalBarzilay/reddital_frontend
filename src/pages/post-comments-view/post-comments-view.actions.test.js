import actions from './actionEnum';
import getPayload from './post-comments-view.actions'

describe('comment view actions', () => {

    it('test get payload', () => {    
      let comment = {id:42, content:"hello?"};
      let post = {id:42, title:"title", content:"hello?", upVotes:10, downVotes:15}
      let txt = "hello!";
      

      expect(getPayload().TOGGLE_REPLY())           .toStrictEqual({type: actions.TOGGLE_REPLY      , payload: {}});
      expect(getPayload().TOGGLE_EDIT())            .toStrictEqual({type: actions.TOGGLE_EDIT       , payload: {}});
      
      expect(getPayload().COMMENT_CHANGE(comment))  .toStrictEqual({type: actions.COMMENT_CHANGE    , payload: comment});
      expect(getPayload().TITLE_CHANGE(txt))  .toStrictEqual({type: actions.TITLE_CHANGE    , payload: txt});
      expect(getPayload().CONTENT_CHANGE(txt))  .toStrictEqual({type: actions.CONTENT_CHANGE    , payload: txt});
    });       
});