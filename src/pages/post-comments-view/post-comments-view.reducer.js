import actions from './actionEnum';

/** The inital state of the reducer, will return by callig reducer with no arguments */
const INITIAL_STATE = {showCommentReplyBox:false, showCommentEditBox:false, titleText:"", contentText:"", commentingText:"", post:null};


  // ------------------------------------------- exported methods -------------------------------------------


/**
 * the login reducer
 * @param {*} state the current state
 * @param {*} param1 the action to perform, that was recived from getPayload.
 * @returns 
 */
const reducer = ( state=INITIAL_STATE, {type, payload} = {}) => {
    switch (type) {

      case actions.TOGGLE_REPLY:
        return {...state, showCommentReplyBox: !state.showCommentReplyBox};

      case actions.TOGGLE_EDIT:
        return {...state, showCommentEditBox: !state.showCommentEditBox};

      case actions.COMMENT_CHANGE:
        return {...state, commentingText: payload};

      case actions.TITLE_CHANGE:
        return {...state, titleText: payload};

      case actions.CONTENT_CHANGE:
        return {...state, contentText: payload};

      default: 
        return state;
    }
  };
  


export default reducer;