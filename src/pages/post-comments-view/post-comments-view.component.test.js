import React from 'react';
import { shallow } from 'enzyme';

import PostCommentView from './post-comments-view.component';



//test to see that all the elements are bing rendered
describe('post-comments-view component', () => {

    // create a new component
    const createComponent = () =>
        shallow(
            <PostCommentView
                post = {{
                  id: 77,
                  title: 'fir the one thiks Spongebob Squarepants will be a good president of the USA',
                  content:"I'll have to disagree. Spongebob Squarepants will be a TERIBLE presidet.", 
                  creation:1637427478, 
                  lastUpdated:1637477478,
                  username:"TalKing",
                  upVotes:42,
                  downVotes:69,
                  emotes:[],
                  comments:[],
                }}
            />
  );

    //test to see if components exist
    it('renders comment-view', () => {     
      const comp = createComponent();

      expect(comp.find("div").length).toBe(5);
      expect(comp.find("h2").length).toBe(1);
      expect(comp.find("h3").length).toBe(1);
      expect(comp.find("h5").length).toBe(1);
      expect(comp.find("button").length).toBe(1);     
    });       
});