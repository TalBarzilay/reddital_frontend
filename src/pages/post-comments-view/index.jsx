import React, {useEffect, useReducer, useCallback} from 'react';

import { withRouter } from '../../components/withRouter';

import { connect } from 'react-redux';
import { INIT_POSTS, POST_COMMENT, UPDATE} from '../../redux/posts-of-sub/posts-of-sub.actions';
import { selectPost } from '../../redux/posts-of-sub/posts-of-sub.selectors';
import { selectMessageService, selectPostService, selectAuthenticationService } from '../../redux/service/service.selectors';
import { ADD_COMMENT } from '../../redux/todays-activities/todays-activities.actions';

import reducer from './post-comments-view.reducer';
import getPayload from './post-comments-view.actions';

import PostCommentsView from './post-comments-view.component';

const PostCommentsViewIndex = ({post, id, messageService, postService, authService,  POST_INIT, POST_COMMENT, UPDATE, ADD_COMMENT}) => {

    // ------------------------------------------- state -------------------------------------------
    
    const [state, dispatch] = useReducer(reducer, reducer());

    // --------------------------------- init state ---------------------------------

    const initPost = useCallback(async id => {
        try {
            const response = await postService.getPost(id);
            POST_INIT(response.data);
        } catch(err){
            messageService.err(err);   
        }
    }, [postService, messageService, POST_INIT]);

    useEffect(()=>{       
        
        if(!post)
           initPost(id); 
        else  {
           dispatch(getPayload().TITLE_CHANGE(post.title));
           dispatch(getPayload().CONTENT_CHANGE(post.content));
        }

    }, [initPost, id, post]);

    useEffect(()=>{     
        messageService.info('Did you know?', 'Click on a comment to hide all of its sub comments!')            
    }, [messageService]);


    // ------------------------------------------- functions -------------------------------------------


    const handleComenting = useCallback(async state => {
        let contnet = state.commentingText;
        const postId =  post ? post.id : post;

        if(contnet === ""){
            messageService.error('no content is specified', 'please enter a comment');
        } else {

            try{
                const response = await postService.replyToPost(contnet, postId);

                let newComment = response.data;
                newComment.comments = []; // replace null with empty list

                dispatch(getPayload().TOGGLE_REPLY());
                POST_COMMENT(post.id, newComment);
                ADD_COMMENT(newComment, post.id);
                
                messageService.success('commented successfully!');
            } catch(err){
                messageService.err(err);
            }
        }             
    }, [POST_COMMENT,ADD_COMMENT, messageService, postService, post]);


    const handleEditing = useCallback(async state => {
        let title   = state.titleText;
        let contnet = state.contentText;
        const id    =  post ? post.id : post;

        if(title === ""){
            messageService.error('no title is specified', 'please enter a title');
        } else {

            try{
                const response = await postService.editPost(id, title, contnet);

                UPDATE(response.data);
                dispatch(getPayload().TOGGLE_EDIT());
                
                messageService.success('edited successfully!');
            } catch(err){
                messageService.err(err);
            }
        }             
    }, [UPDATE, messageService, postService, post]);

    
    
    
    const handleUpvote = useCallback(async id => {
        try{
            const response = await postService.upvotePost(id);
            UPDATE(response.data);   
        } catch(err){
            messageService.err(err);
        }
    }, [UPDATE, messageService, postService]);

    const handleDownvote = useCallback(async id => {
        try{
            const response = await postService.downvotePost(id);
            UPDATE(response.data);   
        } catch(err){
            messageService.err(err);
        }
    }, [UPDATE, messageService, postService]);

    const handlelock = useCallback(async () => {
        try{
            const response = await postService.blockPost(id);
            response.data.blocked?
                messageService.success('post was blocked') :
                messageService.success('post was unblocked');
        } catch(err){
            messageService.err(err);
        }
    }, [id, messageService, postService]);


    // ------------------------------------------- return -------------------------------------------

    const user = authService.getLoggedUser();

    return (
        <PostCommentsView
            {...state}
            post = {post}

            OnToggleReplyClick  = {() => dispatch(getPayload().TOGGLE_REPLY())}
            OnToggleEditClick   = {() => dispatch(getPayload().TOGGLE_EDIT())}

            onCommentChange     = {event => dispatch(getPayload().COMMENT_CHANGE(event.target.value))}
            onTitleChange       = {event => dispatch(getPayload().TITLE_CHANGE(event.target.value))}
            onContentChange     = {event => dispatch(getPayload().CONTENT_CHANGE(event.target.value))}
            
            
            onPostCommentClick  = {() => handleComenting(state)}
            onPostEditClick     = {() => handleEditing(state)}

            onPostLike      = {handleUpvote} 
            onPostDislike   = {handleDownvote}

            onPostlock = {handlelock}
            isAdmin={authService.isAdmin()}
            isOwnPost = { user && post ? user.username === post.username : false}
        />
    )
};


const mapStateToProps = (state, ownProps) => {
    const match             = ownProps.match;
    const id                = parseInt(match.params.postid);
    const post              =  selectPost(state, id);
    const messageService    = selectMessageService(state);
    const postService       = selectPostService(state);
    const authService       = selectAuthenticationService(state);

    return {post, id, messageService, postService, authService};
};

const mapDistpatchToProps = dispatch => ({
    POST_COMMENT    : (postid, comment) => dispatch(POST_COMMENT(postid, comment)),
    UPDATE          : post => dispatch(UPDATE(post)),
    POST_INIT       : post => dispatch(INIT_POSTS([post])),
    ADD_COMMENT     : (comment, postId) => dispatch(ADD_COMMENT(comment, postId)),
});

export default withRouter('/:postid/comments')(connect(mapStateToProps, mapDistpatchToProps)(PostCommentsViewIndex));
