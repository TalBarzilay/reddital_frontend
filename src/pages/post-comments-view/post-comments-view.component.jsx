import React from 'react';

import CommentView from '../../components/comment-view';

import reply from '../../resources/reply.png';
import block from '../../resources/block.png';
import edit from '../../resources/edit.png';

import styles from './post-comments-view.module.css';

import PostEmoteBox from './post-emote-box';

export default ({post, contentText, titleText, showCommentReplyBox, showCommentEditBox,  isAdmin, isOwnPost, onTitleChange, onContentChange, OnToggleReplyClick, OnToggleEditClick, onCommentChange, onPostCommentClick, onPostEditClick, onPostLike, onPostDislike, onPostlock}) => {
    
    if (!post)
        return <div />

    const {id, title, content, creation, username, upVotes, downVotes, emotes, comments} = post;

    return (
        <div className={`${styles.PostCommentsContainer}`}>
            <div className={`${styles.postPriviewContainer}`}>
            
                <div className={`${styles.mainInfoContainer}`}>
                    <h3 className={`${styles.title} shipori-font`}>{title}</h3>
                    <br />
                    <h2 className={`${styles.contentText} shipori-font`}>{content}</h2>
                </div>

                <h5 className={`${styles.creator} shipori-font`}>by {username} at {creation}</h5>

                <div className={`${styles.replybuttonContainer}`} >
                    <button onClick={OnToggleReplyClick} className={`${styles.replybutton}`}><img className={`${styles.replyImg}`} src={reply} alt="" /></button>
                    {isAdmin?
                        <button onClick={onPostlock} className={`${styles.blockbutton}`}><img className={`${styles.blockImg}`} src={block} alt="" /></button>
                        : null
                    }
                    {isAdmin || isOwnPost?
                        <button onClick={OnToggleEditClick} className={`${styles.editbutton}`}><img className={`${styles.editImg}`} src={edit} alt="" /></button>
                        : null
                    }                  
                    <PostEmoteBox id={id} emotes={emotes} upVotes={upVotes} downVotes={downVotes} onPostLike ={onPostLike} onPostDislike={onPostDislike} />
                </div>

            {
                showCommentReplyBox ?
                    <div className={`${styles.replyContainer}`}>
                        <input className={`input ${styles.replyBox}`} placeholder="comment now..." onChange= {onCommentChange}></input>
                        <button className={`buton ${styles.replyOnPostButton}`} onClick={onPostCommentClick}>Comment</button>
                    </div>
                : undefined
            }

            {
                showCommentEditBox ?
                    <div className={`${styles.editContainer}`}>
                        <input className={`input ${styles.titleBox}`} placeholder="title"   value={titleText} onChange= {onTitleChange}></input>
                        <textarea className={`input ${styles.contentBox}`} placeholder="content" value={contentText} onChange= {onContentChange}></textarea>
                        <button className={`buton ${styles.editPostButton}`} onClick={onPostEditClick}>edit</button>
                    </div>
                : undefined
            }
            
         
            </div>

            <div>
                {comments.map(comment => <CommentView key={comment.id} id={comment.id} />)}
            </div>
        </div>
    )
}
