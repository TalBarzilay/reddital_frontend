import React from 'react';

import EmoteBox from '../../../components/emote-box';

import like from '../../../resources/like.png';
import dislike from '../../../resources/dislike.png';

export default ({id, emotes, upVotes, downVotes, onPostLike, onPostDislike }) => {

    const emoteList = [
        {
            id: id + "_179373896689",
            img:like,
            counter: upVotes,
            click:() => onPostLike(id),
            isActivated: emotes.includes("upvote")
        },
        {
            id: id + "_238464873469423490",
            img: dislike,
            counter: downVotes,
            click:() => onPostDislike(id),
            isActivated: emotes.includes("downvote")
        }
    ];


    return (
        <EmoteBox emoteList={emoteList}/>
    )
}
