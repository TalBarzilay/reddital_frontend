import actions from './actionEnum';


/**
 * @returns the payload to send for the reducer function, depending on the type of action
 */
const getPayload = () => ({
    TOGGLE_REPLY        : ()        =>  ({type: actions.TOGGLE_REPLY        , payload: {}}),
    TOGGLE_EDIT         : ()        =>  ({type: actions.TOGGLE_EDIT        , payload: {}}),
    
    COMMENT_CHANGE      : content   =>  ({type: actions.COMMENT_CHANGE      , payload: content}),
    TITLE_CHANGE        : content   =>  ({type: actions.TITLE_CHANGE        , payload: content}),
    CONTENT_CHANGE      : content   =>  ({type: actions.CONTENT_CHANGE      , payload: content}),
});


export default getPayload;