import reducer from './post-comments-view.reducer';
import getPayload from './post-comments-view.actions';

describe('post-comments-view reducer', () => {

    it('reduce no input', () => {     
      expect(reducer()).toStrictEqual({showCommentReplyBox:false, showCommentEditBox:false, showCommentEditBox:false, commentingText:"",titleText:"", contentText:"", post:null});
    });  
    

    it('reduce TOGGLE_REPLY', () => {     
      expect(reducer({showCommentReplyBox:false , showCommentEditBox:false, commentingText:"", titleText:"", contentText:""     , post:null}    ,getPayload().TOGGLE_REPLY())).toStrictEqual({showCommentReplyBox:true  ,showCommentEditBox:false, commentingText:""     ,titleText:"", contentText:"",  post:null});
      expect(reducer({showCommentReplyBox:true  ,showCommentEditBox:false, commentingText:""     ,titleText:"", contentText:"",  post:null}    ,getPayload().TOGGLE_REPLY())).toStrictEqual({showCommentReplyBox:false ,showCommentEditBox:false, commentingText:""     ,titleText:"", contentText:"",  post:null});
      expect(reducer({showCommentReplyBox:true  ,showCommentEditBox:false, commentingText:"aaab" ,titleText:"", contentText:"",  post:{id:42}} ,getPayload().TOGGLE_REPLY())).toStrictEqual({showCommentReplyBox:false ,showCommentEditBox:false, commentingText:"aaab" ,titleText:"", contentText:"",  post:{id:42}});
    });

    it('reduce TOGGLE_EDIT', () => {     
      expect(reducer({showCommentReplyBox:true  , showCommentEditBox:false, commentingText:""     ,titleText:"", contentText:"",  post:null}  ,getPayload().TOGGLE_EDIT())).toStrictEqual({showCommentReplyBox:true  ,showCommentEditBox:true, commentingText:""     ,titleText:"", contentText:"",  post:null});
      expect(reducer({showCommentReplyBox:false ,showCommentEditBox:true, commentingText:""     ,titleText:"", contentText:"",  post:null}    ,getPayload().TOGGLE_EDIT())).toStrictEqual({showCommentReplyBox:false ,showCommentEditBox:false, commentingText:""     ,titleText:"", contentText:"",  post:null});
      expect(reducer({showCommentReplyBox:true ,showCommentEditBox:true, commentingText:"aaab" ,titleText:"", contentText:"",  post:{id:42}} ,getPayload().TOGGLE_EDIT())).toStrictEqual({showCommentReplyBox:true ,showCommentEditBox:false, commentingText:"aaab" ,titleText:"", contentText:"",  post:{id:42}});
    });
    
    it('reduce COMMENT_CHANGE', () => {     
      expect(reducer({showCommentReplyBox:false ,showCommentEditBox:false, commentingText:""     ,titleText:"", contentText:"",  post:null}    ,getPayload().COMMENT_CHANGE("abc")))      .toStrictEqual({showCommentReplyBox:false  ,showCommentEditBox:false, commentingText:"abc"      ,titleText:"", contentText:"",  post:null});
      expect(reducer({showCommentReplyBox:true  ,showCommentEditBox:false, commentingText:"abc"  ,titleText:"", contentText:"",  post:null}    ,getPayload().COMMENT_CHANGE("")))         .toStrictEqual({showCommentReplyBox:true   ,showCommentEditBox:false, commentingText:""         ,titleText:"", contentText:"",  post:null});
      expect(reducer({showCommentReplyBox:true  ,showCommentEditBox:false, commentingText:"aaab" ,titleText:"", contentText:"",  post:{id:42}} ,getPayload().COMMENT_CHANGE("asdfghj")))  .toStrictEqual({showCommentReplyBox:true   ,showCommentEditBox:false, commentingText:"asdfghj"  ,titleText:"", contentText:"",  post:{id:42}});
    }); 

    it('reduce TITLE_CHANGE', () => {     
      expect(reducer({showCommentReplyBox:false ,showCommentEditBox:false, commentingText:""     ,titleText:"", contentText:"",  post:null}    ,getPayload().TITLE_CHANGE("abc")))      .toStrictEqual({showCommentReplyBox:false  ,showCommentEditBox:false, commentingText:""      ,titleText:"abc", contentText:"",  post:null});
      expect(reducer({showCommentReplyBox:true  ,showCommentEditBox:false, commentingText:""  ,titleText:"abc", contentText:"",  post:null}    ,getPayload().TITLE_CHANGE("")))         .toStrictEqual({showCommentReplyBox:true   ,showCommentEditBox:false, commentingText:""         ,titleText:"", contentText:"",  post:null});
      expect(reducer({showCommentReplyBox:true  ,showCommentEditBox:false, commentingText:"" ,titleText:"aaab", contentText:"",  post:{id:42}} ,getPayload().TITLE_CHANGE("asdfghj")))  .toStrictEqual({showCommentReplyBox:true   ,showCommentEditBox:false, commentingText:""  ,titleText:"asdfghj", contentText:"",  post:{id:42}});
    }); 

    it('reduce CONTENT_CHANGE', () => {     
      expect(reducer({showCommentReplyBox:false ,showCommentEditBox:false, commentingText:""     ,titleText:"", contentText:"",  post:null}    ,getPayload().CONTENT_CHANGE("abc")))      .toStrictEqual({showCommentReplyBox:false  ,showCommentEditBox:false, commentingText:""      ,titleText:"", contentText:"abc",  post:null});
      expect(reducer({showCommentReplyBox:true  ,showCommentEditBox:false, commentingText:""  ,titleText:"", contentText:"abc",  post:null}    ,getPayload().CONTENT_CHANGE("")))         .toStrictEqual({showCommentReplyBox:true   ,showCommentEditBox:false, commentingText:""         ,titleText:"", contentText:"",  post:null});
      expect(reducer({showCommentReplyBox:true  ,showCommentEditBox:false, commentingText:"" ,titleText:"", contentText:"aaab",  post:{id:42}} ,getPayload().CONTENT_CHANGE("asdfghj")))  .toStrictEqual({showCommentReplyBox:true   ,showCommentEditBox:false, commentingText:""  ,titleText:"", contentText:"asdfghj",  post:{id:42}});
    }); 
});