const actions = {
    EMAIL_CHANGED : 'EMAIL_CHANGED',
    OLD_PASSWORD_CHANGE : 'OLD_PASSWORD_CHANGE',
    REPEAT_PASSWORD_CHANGE : 'REPEAT_PASSWORD_CHANGE',
    NEW_PASSWORD_CHANGE : 'NEW_PASSWORD_CHANGE',
};

export default actions;