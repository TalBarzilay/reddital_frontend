import actions from './actionEnum';

/** The inital state of the reducer, will return by callig reducer with no arguments */
const INITIAL_STATE = {email:"", oldPassword:"", repeatPassword:"", newPassword:""};


// ------------------------------------------- exported methods -------------------------------------------


/**
 * the login reducer
 * @param {*} state the current state
 * @param {*} param1 the action to perform, that was recived from getPayload.
 * @returns 
 */
const reducer = ( state=INITIAL_STATE, {type, payload} = {}) => {
    switch (type) {

        case actions.EMAIL_CHANGED:
            return {...state, email: payload};

        case actions.OLD_PASSWORD_CHANGE:
            return {...state, oldPassword: payload};

        case actions.REPEAT_PASSWORD_CHANGE:
            return {...state, repeatPassword: payload};

        case actions.NEW_PASSWORD_CHANGE:
            return {...state, newPassword: payload};

        default: 
            return state;
    }
  };
  


export default reducer;