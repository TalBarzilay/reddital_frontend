import reducer from './profile.reducer';
import {EMAIL_CHANGED, OLD_PASSWORD_CHANGE, REPEAT_PASSWORD_CHANGE, NEW_PASSWORD_CHANGE} from './profile.actions';


// test reducer function
describe('profile reducer', () => {

    // test that reducer returns currect inital state without arguments
    it('reduce no argument', () => {
        expect(reducer()) .toStrictEqual({email:"", oldPassword:"", repeatPassword:"", newPassword:""});
    });

    it('reduce EMAIL_CHANGED', () => {
      const txt = "ancdefg";

      expect(reducer(undefined                                                                      , EMAIL_CHANGED(txt)))  .toStrictEqual({email:txt, oldPassword:"", repeatPassword:"", newPassword:""});
      expect(reducer({email:"", oldPassword:"", repeatPassword:"", newPassword:""}                  , EMAIL_CHANGED(txt)))  .toStrictEqual({email:txt, oldPassword:"", repeatPassword:"", newPassword:""});
      expect(reducer({email:"a@a.a", oldPassword:"123", repeatPassword:"321", newPassword:"132"}    , EMAIL_CHANGED(txt)))  .toStrictEqual({email:txt, oldPassword:"123", repeatPassword:"321", newPassword:"132"});
    });

    it('reduce OLD_PASSWORD_CHANGE', () => {
        const txt = "ancdefg";
  
        expect(reducer(undefined                                                                      , OLD_PASSWORD_CHANGE(txt)))  .toStrictEqual({email:"", oldPassword:txt, repeatPassword:"", newPassword:""});
        expect(reducer({email:"", oldPassword:"", repeatPassword:"", newPassword:""}                  , OLD_PASSWORD_CHANGE(txt)))  .toStrictEqual({email:"", oldPassword:txt, repeatPassword:"", newPassword:""});
        expect(reducer({email:"a@a.a", oldPassword:"123", repeatPassword:"321", newPassword:"132"}    , OLD_PASSWORD_CHANGE(txt)))  .toStrictEqual({email:"a@a.a", oldPassword:txt, repeatPassword:"321", newPassword:"132"});
    });

    it('reduce REPEAT_PASSWORD_CHANGE', () => {
        const txt = "ancdefg";
  
        expect(reducer(undefined                                                                      , REPEAT_PASSWORD_CHANGE(txt)))  .toStrictEqual({email:"", oldPassword:"", repeatPassword:txt, newPassword:""});
        expect(reducer({email:"", oldPassword:"", repeatPassword:"", newPassword:""}                  , REPEAT_PASSWORD_CHANGE(txt)))  .toStrictEqual({email:"", oldPassword:"", repeatPassword:txt, newPassword:""});
        expect(reducer({email:"a@a.a", oldPassword:"123", repeatPassword:"321", newPassword:"132"}    , REPEAT_PASSWORD_CHANGE(txt)))  .toStrictEqual({email:"a@a.a", oldPassword:"123", repeatPassword:txt, newPassword:"132"});
    });

    it('reduce NEW_PASSWORD_CHANGE', () => {
        const txt = "ancdefg";
  
        expect(reducer(undefined                                                                      , NEW_PASSWORD_CHANGE(txt)))  .toStrictEqual({email:"", oldPassword:"", repeatPassword:"", newPassword:txt});
        expect(reducer({email:"", oldPassword:"", repeatPassword:"", newPassword:""}                  , NEW_PASSWORD_CHANGE(txt)))  .toStrictEqual({email:"", oldPassword:"", repeatPassword:"", newPassword:txt});
        expect(reducer({email:"a@a.a", oldPassword:"123", repeatPassword:"321", newPassword:"132"}    , NEW_PASSWORD_CHANGE(txt)))  .toStrictEqual({email:"a@a.a", oldPassword:"123", repeatPassword:"321", newPassword:txt});
    });
});