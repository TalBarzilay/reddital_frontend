import React from 'react';
import { mount } from 'enzyme';
import {BrowserRouter as Router} from 'react-router-dom';

import {Provider} from 'react-redux';
import store from '../../redux/store';  

import ProfileIndex from './index';


jest.mock('../../services/authentication.service', () => ({
    getInstance: () => ({
        getLoggedUser    : () => ({email:"banana"}),
    })  
}));



jest.mock('../../services/message.service', () => jest.fn().mockImplementation(() => ({
    info            : jest.fn(),
    success         : jest.fn(),
    warn            : jest.fn(),
    error           : jest.fn(),
    err             : jest.fn(),
    addNotification : jest.fn(),
  })));
  


jest.mock('../../services/user.service', () => jest.fn());
jest.mock('../../services/web-request.service', () => jest.fn());


//test to see that all the elements are bing rendered
describe('profile index', () => {

    // create a new login component
    const createIndex = () =>
        mount(
            <Provider store={store}>
                <Router>
                    <ProfileIndex/>
                </Router> 
            </Provider>
        );

    const getComp = index => index.find('Profile').at(0);

    it('email changed', () => {
        let value;     
        const input_at = profile => profile.find('input').at(0);
        const prop = "email";


        const index = createIndex();
        let profile = getComp(index);

        let textbox = input_at(profile);


        expect(textbox.length).toEqual(1);

        
        value = "a@a.a"; 
        textbox.simulate("change", { target: { value } });
        profile = getComp(index);
        expect(profile.prop(prop)).toEqual(value);

        value = "admin@a.a"; 
        textbox.simulate("change", { target: { value } });
        profile = getComp(index);
        expect(profile.prop(prop)).toEqual(value);
    });


    it('old password changed', () => {
        let value;     
        const input_at = profile => profile.find('input').at(1);
        const prop = "oldPassword";


        const index = createIndex();
        let profile = getComp(index);

        let textbox = input_at(profile);


        expect(textbox.length).toEqual(1);

        
        value = "123456"; 
        textbox.simulate("change", { target: { value } });
        profile = getComp(index);
        expect(profile.prop(prop)).toEqual(value);

        value = "12345678909875"; 
        textbox.simulate("change", { target: { value } });
        profile = getComp(index);
        expect(profile.prop(prop)).toEqual(value);
    });


    it('new password changed', () => {
        let value;     
        const input_at = profile => profile.find('input').at(2);
        const prop = "newPassword";


        const index = createIndex();
        let profile = getComp(index);

        let textbox = input_at(profile);


        expect(textbox.length).toEqual(1);

        
        value = "123456"; 
        textbox.simulate("change", { target: { value } });
        profile = getComp(index);
        expect(profile.prop(prop)).toEqual(value);

        value = "12345678909875"; 
        textbox.simulate("change", { target: { value } });
        profile = getComp(index);
        expect(profile.prop(prop)).toEqual(value);
    });



    it('repeat password changed', () => {
        let value;     
        const input_at = profile => profile.find('input').at(3);
        const prop = "repeatPassword";


        const index = createIndex();
        let profile = getComp(index);

        let textbox = input_at(profile);


        expect(textbox.length).toEqual(1);

        
        value = "123456"; 
        textbox.simulate("change", { target: { value } });
        profile = getComp(index);
        expect(profile.prop(prop)).toEqual(value);

        value = "12345678909875"; 
        textbox.simulate("change", { target: { value } });
        profile = getComp(index);
        expect(profile.prop(prop)).toEqual(value);
    });

});