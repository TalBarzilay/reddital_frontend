import  actions from './actionEnum';


export const EMAIL_CHANGED = email =>
    ({type: actions.EMAIL_CHANGED , payload:email});

export const OLD_PASSWORD_CHANGE = password =>
    ({type: actions.OLD_PASSWORD_CHANGE , payload:password});

export const REPEAT_PASSWORD_CHANGE = password =>
    ({type: actions.REPEAT_PASSWORD_CHANGE , payload:password});

export const NEW_PASSWORD_CHANGE = password =>
    ({type: actions.NEW_PASSWORD_CHANGE , payload:password});