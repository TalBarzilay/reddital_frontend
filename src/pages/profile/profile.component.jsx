import React from 'react';
import styles from './profile.module.css';

const Profile = ({email, onEmailChange, onOldPasswordChange, onRepeatsswordChange, onNewPasswordChange, onEditProfleClick, onChangePasswordClick}) => {

    return (
        <div className= {`${styles.profile} form-block`}>
           
            <h1 className={`${styles.title} lobster-font`}>Edit Profile</ h1>



            <h3 className={`${styles.secondaryTitle} lobster-font`}>Change Info</ h3>

            <input id="email" className={`input ${styles.textinput}`} onChange={onEmailChange} placeholder="email" value={email} type="text"></input> 

            <button className={`buton ${styles.editProfileButon} sil-font`} onClick={onEditProfleClick}>Edit Info</button>

            <br/>
            <br/>

            <h3 className={`${styles.secondaryTitle} lobster-font`}>Change Password</ h3>

            <input id="old-password" className={`input ${styles.textinput}`} onChange={onOldPasswordChange}  placeholder= "old password"        type="password"></input>
            <input id="new-password" className={`input ${styles.textinput}`} onChange={onNewPasswordChange}  placeholder= "new password"        type="password"></input> 
            <input id="rep-password" className={`input ${styles.textinput}`} onChange={onRepeatsswordChange} placeholder= "repeat new password" type="password"></input>
           
            <button className={`buton ${styles.changePasswordButon} sil-font`} onClick={onChangePasswordClick}>Change Password</button>


        </div>
    )
};

export default Profile;