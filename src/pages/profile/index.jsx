import React, { useReducer, useCallback, useEffect } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';

import Profile from './profile.component';
import reducer from './profile.reducer';
import {EMAIL_CHANGED, OLD_PASSWORD_CHANGE, REPEAT_PASSWORD_CHANGE, NEW_PASSWORD_CHANGE} from './profile.actions';

import { useSelector } from 'react-redux';
import {selectMessageService, selectUserService, selectAuthenticationService} from '../../redux/service/service.selectors';


const ProfileIndex = () => {

  // ------------------------------------------- state -------------------------------------------
  
  const [state, dispatch] = useReducer(reducer, reducer());

  const messageService        = useSelector(selectMessageService);
  const userService           = useSelector(selectUserService);
  const authenticationService = useSelector(selectAuthenticationService);

  const navigate = useNavigate();

  // ------------------------------------------- init state -------------------------------------------

  useEffect(() => {
    const user = authenticationService.getLoggedUser();

    if(user)
      dispatch(EMAIL_CHANGED(user.email)); // init email
  }, [authenticationService]);

  // ------------------------------------------- action handlers -------------------------------------------

  /**
   * handle pressing the update profile button
   * @param {*} state the current state when clicking the button
   */
  const handleUpdateProfile = useCallback(async state => {

    if(state.email !== ""){
      //inputs are valid, contacting the server...

      try {
        const response = await userService.editProfile(state.email);
        authenticationService.setUser(response.data);
        
        messageService.success('profile was edited successfully!');

        navigate('/profile');
      } catch(err) {
        messageService.err(err);
      }
    } else {
      messageService.error('email is missing', 'please provide valid email.');
    }
  }, [userService, messageService, authenticationService, navigate]);



   /**
   * handle pressing the change password button
   * @param {*} state the current state when clicking the button
   */
    const handleChangePassword = useCallback(async state => {

        if(state.oldPassword !== "" && state.newPassword !== "" && state.repeatPassword !== ""){
          //inputs are valid, contacting the server...

          if(state.newPassword !== state.repeatPassword){
            messageService.error('passwords do not match!');
            return;   
          }

          if( state.newPassword.length < 6 ){
            messageService.error('new password is too short!');
            return;   
          }
    
          try {
            await userService.changePassword(state.newPassword, state.oldPassword);

            authenticationService.logout();    
            messageService.success('password was changed successfully!', 'please login again');

            navigate('/login');
          } catch(err) {
            messageService.err(err);
          }
        } else {
          messageService.error('info is missing', 'please provide valid info.');
        }
      }, [userService, messageService, authenticationService, navigate]);

  // ------------------------------------------- return statement -------------------------------------------

  return authenticationService.getLoggedUser() == null ?
    (<Navigate to='/login'/>) : (
      <Profile
        {...state}


        onEmailChange       ={event      => dispatch(EMAIL_CHANGED(event.target.value))}
        onOldPasswordChange ={event      => dispatch(OLD_PASSWORD_CHANGE(event.target.value))}
        onNewPasswordChange ={event      => dispatch(NEW_PASSWORD_CHANGE(event.target.value))}
        onRepeatsswordChange={event      => dispatch(REPEAT_PASSWORD_CHANGE(event.target.value))}
        
        onEditProfleClick       ={() => handleUpdateProfile(state)}
        onChangePasswordClick   ={() => handleChangePassword(state)}
      />);
};






export default  ProfileIndex;
