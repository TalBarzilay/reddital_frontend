import React from 'react';
import { shallow } from 'enzyme';

import Profile from './profile.component';



//test to see that all the elements are bing rendered
describe('profile component', () => {
  
  const createComp = () =>
    shallow(
      <Profile
      />
    );

    //test to see if components exist
    it('renders profile', () => {
      const comp = createComp();

      expect(comp.find("div").length).toBe(1);
      expect(comp.find("input").length).toBe(4);
      expect(comp.find("button").length).toBe(2);
      expect(comp.find("h1").length).toBe(1);
      expect(comp.find("h3").length).toBe(2);
    });
        
});