import  actions from './actionEnum';
import {EMAIL_CHANGED, OLD_PASSWORD_CHANGE, REPEAT_PASSWORD_CHANGE, NEW_PASSWORD_CHANGE} from './profile.actions';

describe('profile actions', () => {

    it('EMAIL_CHANGED', () => {
        const txt = "a@a.a";
        expect(EMAIL_CHANGED(txt)).toStrictEqual({type: actions.EMAIL_CHANGED , payload:txt});
    });

    it('OLD_PASSWORD_CHANGE', () => {
        const txt = "a@a.a";
        expect(OLD_PASSWORD_CHANGE(txt)).toStrictEqual({type: actions.OLD_PASSWORD_CHANGE , payload:txt});
    });

    it('REPEAT_PASSWORD_CHANGE', () => {
        const txt = "a@a.a";
        expect(REPEAT_PASSWORD_CHANGE(txt)).toStrictEqual({type: actions.REPEAT_PASSWORD_CHANGE , payload:txt});
    });

    it('NEW_PASSWORD_CHANGE', () => {
        const txt = "a@a.a";
        expect(NEW_PASSWORD_CHANGE(txt)).toStrictEqual({type: actions.NEW_PASSWORD_CHANGE , payload:txt});
    });

});