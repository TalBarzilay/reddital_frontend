import React, { useReducer, useCallback } from 'react';
import Login from './login.component';
import reducer from './login.reducer';
import getPayload from './login.actions';

import { useSelector } from 'react-redux';
import { selectMessageService, selectAuthenticationService } from '../../redux/service/service.selectors';

import { useNavigate, Navigate } from 'react-router-dom';

const LoginIndex = () => {

  // ------------------------------------------- state -------------------------------------------
 
  
  const [state, dispatch] = useReducer(reducer, reducer());
  const navigate = useNavigate();

  const messageService        = useSelector(state => selectMessageService(state));
  const authenticationService = useSelector(state =>selectAuthenticationService(state));

  // ------------------------------------------- action handlers -------------------------------------------
 

  /**
   * handle pressing the login vakue
   * @param {*} state the current state when clickin the buttom
   */
  const handleLogin = useCallback(async state => {
    if(state.userErr === "" && state.passwordErr === ""){
      //inputs are valid, contacting the server....

      try{
        await authenticationService.login(state.username, state.password);
        messageService.success('Logged in successfully');
        
        navigate('/subredditals'); // rereder, so the header will update
        
      } catch(err) {
        messageService.err(err);
      }
    } else {
      messageService.error('cradentials are invalid', 'please provide valid credentials');  // the inputs atre not valid
    }
  }, [messageService, authenticationService, navigate]);

  /**
   * handle clickin the redirect to register button
   */
  const handleSignupRedirect = useCallback(() => {
    navigate('/signup');
  }, [navigate]);

  // ------------------------------------------- return statement -------------------------------------------
 
  return authenticationService.getLoggedUser() != null ?
    (<Navigate to='/subredditals'/>) : (
  <Login
     {...state}

    onUserNameChange  = {event       => dispatch(getPayload().USERNAME_CHANGE(event.target.value))}
    onPasswordChange  = {event       => dispatch(getPayload().PASSWORD_CHANGE(event.target.value))}

    onLogin           = {()       => handleLogin(state)}
    signup            = {handleSignupRedirect}
  />);
};







export default LoginIndex;
