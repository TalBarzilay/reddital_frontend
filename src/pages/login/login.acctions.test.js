import  actions from './actionEnum';
import getPayload from './login.actions'

//test getPayload function
describe('login getPayload', () => {

    it('getPayload', () => {
      const username = "banana";
      const password = "12345678";
  
      expect(getPayload().USERNAME_CHANGE(username)).toStrictEqual({type: actions.USERNAME_CHANGE , payload:username});
      expect(getPayload().PASSWORD_CHANGE(password)).toStrictEqual({type: actions.PASSWORD_CHANGE , payload:password});
    });
  });