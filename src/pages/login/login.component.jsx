import React from 'react';
import styles from './login.module.css';

const Login = ({onUserNameChange, onPasswordChange, onLogin, signup, userErr, passwordErr}) => {

    return (
        <div className= {`${styles.login} center form-block`}>
           
            <h1 className={`${styles.title} lobster-font`}>Login</ h1>

            <input id="username" className={`input ${styles.textinput}`} onChange={onUserNameChange} placeholder="username" type="text"></input> 
            <br />
            <label id="username-validator" className={`validator`}>{userErr}</label>

            <br />

            <input id="password" className={`input ${styles.textinput}`} onChange={onPasswordChange} placeholder="password" type="password"></input> 
            <br />
            <label id="password-validator" className={`validator`}>{passwordErr}</label>

            <br />
            <br />
            <button id="login" className={`buton ${styles.loginButon} sil-font`} onClick={onLogin} type="submit">Login</button>

            <button id="signupRedirect" onClick={signup} className={`${styles.redirectSignup} link`}>don't have one? register now!</button>
        </div>
    )
};

export default Login;