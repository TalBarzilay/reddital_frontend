import actions from './actionEnum';

/** The inital state of the reducer, will return by callig reducer with no arguments */
const INITIAL_STATE = {username:"",password:"" , userErr:" ",passwordErr:" "};

// ------------------------------------------- validators -------------------------------------------

  /**
   * @returns an error mesage for a the username, empty string if valid
   */
   const validateNotempty = str => {
    if (str === "") {
      return "* can't be empty";
    }

    return "";
  };


  // ------------------------------------------- exported methods -------------------------------------------


/**
 * the login reducer
 * @param {*} state the current state
 * @param {*} param1 the action to perform, that was recived from getPayload.
 * @returns 
 */
const reducer = ( state=INITIAL_STATE, {type, payload} = {}) => {
    switch (type) {

      case actions.USERNAME_CHANGE: // user name has changed
        return {...state, username: payload, userErr: validateNotempty(payload)};

      case actions.PASSWORD_CHANGE: // password has changed
        return {...state, password: payload, passwordErr:validateNotempty(payload)};

      default: 
        return state;
    }
  };
  


export default reducer;