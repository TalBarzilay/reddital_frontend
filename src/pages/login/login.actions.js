import actions from './actionEnum';


/**
 * @returns the payload to send for the reducer function, depending on the type of action
 */
const getPayload = () => ({
    USERNAME_CHANGE   : newVal  =>  ({type: actions.USERNAME_CHANGE , payload: newVal}),
    PASSWORD_CHANGE   : newVal  =>  ({type: actions.PASSWORD_CHANGE , payload: newVal}),
});


export default getPayload;