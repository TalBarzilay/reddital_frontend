const actions = {
    SUBREDDITAL_CHANGED : 'SUBREDDITAL_CHANGED',
    CATEGORY_CHANGED    : 'CATEGORY_CHANGED',
    INIT_CATEGORIES     : 'INIT_CATEGORIES'
};

export default actions;