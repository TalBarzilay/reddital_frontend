import React from 'react';
import styles from './create-subreddital.module.css';

import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

const CreateSubbredial = ({onCategoryChanged, onSubbredditalChanged, onSubmit, categories}) => {

    return (
        <div className= {`${styles.container}`}>
            <div className= {`${styles.createBlock} form-block`}>
           
                <h1 className={`${styles.title} lobster-font`}>Create Subreddital</ h1>

                <input className={`input ${styles.textinput}`} onChange={onSubbredditalChanged} placeholder="Subreddital" type="text"></input> 

                <Dropdown className= {`${styles.dropList}`} options={categories} onChange={onCategoryChanged} placeholder="Select category" />
                
                <button className={`${styles.createButon} buton sil-font`} onClick={onSubmit} type="submit">Create</button>
            </div>
        </div>
    )
};

export default CreateSubbredial;