import  actions from './actionEnum';

export const SUBREDDITAL_CHANGED = subreddital => 
    ({type: actions.SUBREDDITAL_CHANGED , payload:subreddital});

export const CATEGORY_CHANGED = category =>
    ({type: actions.CATEGORY_CHANGED, payload:category});

export const INIT_CATEGORIES = categories =>
    ({type: actions.INIT_CATEGORIES, payload:categories});