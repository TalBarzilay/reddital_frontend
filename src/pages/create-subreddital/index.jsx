import React, { useReducer, useCallback, useEffect } from 'react';
import CreateSubreddital from './create-subreddital.component';
import reducer from './create-subreddital.reducer';
import {CATEGORY_CHANGED, SUBREDDITAL_CHANGED, INIT_CATEGORIES } from './create-subreddital.actions';

import { useSelector } from 'react-redux';
import {selectCategoryService, selectMessageService, selectAuthenticationService } from '../../redux/service/service.selectors';

import { useNavigate, Navigate } from 'react-router-dom';


const CreateSubredditalIndex = () => {

  // ------------------------------------------- state -------------------------------------------
 
  
  const [state, dispatch] = useReducer(reducer, reducer());

  const navigate = useNavigate();

  const categoryService         = useSelector(selectCategoryService);
  const messageService          = useSelector(selectMessageService);
  const authenticationService   = useSelector(selectAuthenticationService);


  // ------------------------------------------- initialization -------------------------------------------

  useEffect(() => {

    const init = async () =>{

      try{
        const categories = await categoryService.edditablCategories();
        dispatch(INIT_CATEGORIES(categories));
      } catch(err){
        messageService.err(err)
      }

    };

    init();

  }, [categoryService, messageService]);

  // ------------------------------------------- action handlers -------------------------------------------
 

  /**
   * handle pressing the login vakue
   * @param {*} state the current state when clickin the buttom
   */
  const handleCreate = useCallback(async (category, subreddital) => {
    if(category !== "" && subreddital !== ""){
      //inputs are valid, contacting the server....

      try{
        await categoryService.createSub(subreddital, category);
        messageService.success('created subreddital in successfully');
        
        navigate('/subredditals');
        
      } catch(err) {
        messageService.err(err);
      }
    } else {
      messageService.error('info is invalid', 'please provide valid subreddital');
    }
  }, [messageService, categoryService, navigate]);


  // ------------------------------------------- return statement -------------------------------------------
 
  return !authenticationService.isAdmin() ?
    (<Navigate to='/subredditals'/>) : (
        <CreateSubreddital
            {...state}

            onCategoryChanged     = {item       => dispatch(CATEGORY_CHANGED(item.value))}
            onSubbredditalChanged = {event      => dispatch(SUBREDDITAL_CHANGED(event.target.value))}

            onSubmit              = {()       => handleCreate(state.category, state.subreddital)}
        />);
};







export default CreateSubredditalIndex;
