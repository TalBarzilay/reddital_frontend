import actions from './actionEnum';

/** The inital state of the reducer, will return by callig reducer with no arguments */
const INITIAL_STATE = {category:"", subreddital:"", categories: []};


// ------------------------------------------- exported methods -------------------------------------------


/**
 * the login reducer
 * @param {*} state the current state
 * @param {*} param1 the action to perform, that was recived from getPayload.
 * @returns 
 */
const reducer = ( state=INITIAL_STATE, {type, payload} = {}) => {
    switch (type) {

        case actions.CATEGORY_CHANGED:
            return {...state, category: payload};

        case actions.SUBREDDITAL_CHANGED:
            return {...state, subreddital: payload}; 

        case actions.INIT_CATEGORIES:
            return {...state, categories:payload}

        default: 
            return state;
    }
  };
  


export default reducer;