import React from 'react';
import { shallow } from 'enzyme';

import CreateSubreddital from './create-subreddital.component';



//test to see that all the elements are bing rendered
describe('create-subreddital component', () => {
  
  // create a new component
  const createComp = () =>
    shallow(
      <CreateSubreddital
        onCategoryChanged   = {event       => {}}
        onSubmit            = {event       =>{}}
      />
    );

    //test to see if components exist
    it('renders component', () => {
      const component = createComp();

      expect(component.find("div")      .length).toBe(2);
      expect(component.find("input")    .length).toBe(1);
      expect(component.find("h1")       .length).toBe(1);
      expect(component.find("button")   .length).toBe(1);
      expect(component.find("Dropdown")   .length).toBe(1);
    });
        
});