import  actions from './actionEnum';
import {SUBREDDITAL_CHANGED, CATEGORY_CHANGED, INIT_CATEGORIES} from './create-subreddital.actions';

describe('create-subreddital actions', () => {

    it('SUBREDDITAL_CHANGED', () => {
      const subreddital = "Minecraft";

      expect(SUBREDDITAL_CHANGED(subreddital)).toStrictEqual({type: actions.SUBREDDITAL_CHANGED , payload:subreddital});
    });

    it('CATEGORY_CHANGED', () => {
      const category = "Gaming";

      expect(CATEGORY_CHANGED(category)).toStrictEqual({type: actions.CATEGORY_CHANGED , payload:category});
    });


    it('INIT_CATEGORIES', () => {
      const categories = ["Gaming", "Tv", "School"];

      expect(INIT_CATEGORIES(categories)).toStrictEqual({type: actions.INIT_CATEGORIES , payload:categories});
    });
});