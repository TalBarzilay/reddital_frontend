import reducer from './create-subreddital.reducer';
import {SUBREDDITAL_CHANGED, CATEGORY_CHANGED, INIT_CATEGORIES} from './create-subreddital.actions';


// test reducer function
describe('create-subreddital reducer', () => {

    // test that reducer returns currect inital state without arguments
    it('reduce no argument', () => {
        expect(reducer()) .toStrictEqual({category:"", subreddital:"", categories:[]});
    });

    //test reducer's SUBREDDITAL_CHANGED action
    it('reduce SUBREDDITAL_CHANGED', () => {
      const subreddital = "Terreria";

      expect(reducer(undefined                                              , SUBREDDITAL_CHANGED(subreddital)))  .toStrictEqual({category:"",subreddital, categories:[]});
      expect(reducer({category:"", subreddital:"Gaming", categories:[]}     , SUBREDDITAL_CHANGED(subreddital)))  .toStrictEqual({category:"",subreddital, categories:[]});
      expect(reducer({category:"old", subreddital:"banana", categories:[]}  , SUBREDDITAL_CHANGED(subreddital)))  .toStrictEqual({category:"old",subreddital, categories:[]});
  });

    //test reducer's SUBREDDITAL_CHANGED action
    it('reduce CATEGORY_CHANGED', () => {
        const category = "Gaming";
  
        expect(reducer(undefined                                            , CATEGORY_CHANGED(category)))  .toStrictEqual({category, subreddital:"", categories:[]});
        expect(reducer({category:"", subreddital:"Gaming", categories:[]}   , CATEGORY_CHANGED(category)))  .toStrictEqual({category, subreddital:"Gaming", categories:[]});
        expect(reducer({category:"old", subreddital:"banana", categories:[]}, CATEGORY_CHANGED(category)))  .toStrictEqual({category, subreddital:"banana", categories:[]});
    });

    //test reducer's SUBREDDITAL_CHANGED action
    it('reduce INIT_CATEGORIES', () => {
        const categories = ["Gaming", "Tv", "School"];
  
        expect(reducer(undefined                                             , INIT_CATEGORIES(categories)))  .toStrictEqual({category:"",subreddital:"", categories});
        expect(reducer({subreddital:"", category:"Gaming", categories:[]}    , INIT_CATEGORIES(categories)))  .toStrictEqual({category:"Gaming",subreddital:"", categories});
        expect(reducer({subreddital:"banana", category:"old", categories:[]} , INIT_CATEGORIES(categories)))  .toStrictEqual({category:"old",subreddital:"banana", categories});
    });

});