import React from 'react';
import styles from './todays-activites.module.css';

import PostPriview from '../../components/post-preview';
import CommentView from '../../components/comment-view';

export default ({posts, comments, commentClick}) => {
    return (
        <div className={`${styles.Container}`}>

            <h1 className={`${styles.LastActionsTitle}`}>Last Actions</h1>

            <h1 className={`${styles.createPostsTitle}`}>Created Posts :</h1>

            <div className={`${styles.postsContainer}`}>
                {posts.map(post => <PostPriview key={post.id} {...post} />)}
            </div>

            <h1 className={`${styles.createCommentsTitle}`}>Created Comments :</h1>

            <div>
                {comments.map(comm => 

                    <div key={comm.comment.id} className={`${styles.postRedirect}`} onClick={() => commentClick(comm.postId)}>
                        <CommentView comment={comm.comment} recursiveRender={false} />
                    </div>
                )}
            </div>
        </div>
    )
}
