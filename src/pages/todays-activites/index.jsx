import React, { useCallback } from 'react';

import TodaysActivites from './todays-activites.component';

import { Navigate, useNavigate } from 'react-router-dom';

import { useSelector } from 'react-redux';
import {selectAuthenticationService} from '../../redux/service/service.selectors';
import { selectPosts, selectComments } from '../../redux/todays-activities/todays-activities.selectors';

const TodaysActivitesIndex = () => {

    // ------------------------------------------- state -------------------------------------------
    
    const navigate = useNavigate();

    const posts                 = useSelector(state => selectPosts(state));
    const comments              = useSelector(state => selectComments(state));
    const authenticationService = useSelector(state => selectAuthenticationService(state));


    // ------------------------------------------- functions -------------------------------------------

      const navigatePost = useCallback(postId => navigate(`/${postId}/comments`),[navigate]);
    
    // ------------------------------------------- return statement -------------------------------------------



    return authenticationService.getLoggedUser() == null ?
        (<Navigate to='/login'/>) : (
        <TodaysActivites 
            posts           = {posts}
            comments        = {comments}
            commentClick    = {navigatePost}
        />
    )
};






export default TodaysActivitesIndex;
