import React from 'react';
import { shallow } from 'enzyme';

import TodaysActivites from './todays-activites.component';

describe('todays-activtes component', () => {

    const createComponent = () =>
        shallow(
        <TodaysActivites
            posts = {[]} 
            comments = {[]}  
        />
    );

    // test that reducer returns currect inital state without arguments
    it('render todays-activtes', () => {
        const comp = createComponent();

        expect(comp.find("div").length).toBe(3);
    });

});