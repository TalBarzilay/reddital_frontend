import  actions from './actionEnum';
import {CATEGORY_CHANGED} from './create-category.actions';

//test getPayload function
describe('create-category actions', () => {

    it('CATEGORY_CHANGED', () => {
        const category = "Gaming";

      expect(CATEGORY_CHANGED(category)).toStrictEqual({type: actions.CATEGORY_CHANGED , payload:category});
    });
  });