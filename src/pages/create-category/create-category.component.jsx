import React from 'react';
import styles from './create-category.module.css';

const CreateCategory = ({onCategoryChanged, onSubmit}) => {

    return (
        <div className= {`${styles.container}`}>
            <div className= {`${styles.createBlock} form-block`}>
           
                <h1 className={`${styles.title} lobster-font`}>Create Category</ h1>

                <div className= {`${styles.textButtonContainer}`}>
                    <input className={`input ${styles.textinput}`} onChange={onCategoryChanged} placeholder="category" type="text"></input> 
                    <button className={`${styles.createButon} buton sil-font`} onClick={onSubmit} type="submit">Create</button>
                </div>
            </div>
        </div>
    )
};

export default CreateCategory;