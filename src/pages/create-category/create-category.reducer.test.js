import reducer from './create-category.reducer';
import {CATEGORY_CHANGED} from './create-category.actions';


// test reducer function
describe('create-category reducer', () => {

    // test that reducer returns currect inital state without arguments
    it('reduce no argument', () => {
        expect(reducer()) .toStrictEqual({category:""});
    });

    //test reducer's USERNAME_CHANGE action
    it('reduce CATEGORY_CHANGED', () => {
      const category = "Gaming";

      expect(reducer(undefined        , CATEGORY_CHANGED(category)))  .toStrictEqual({category});
      expect(reducer({category:""}    , CATEGORY_CHANGED(category)))  .toStrictEqual({category});
      expect(reducer({category:"old"} , CATEGORY_CHANGED(category)))  .toStrictEqual({category});
  });
});