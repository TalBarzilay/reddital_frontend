import React from 'react';
import { shallow } from 'enzyme';

import CreateCategory from './create-category.component';



//test to see that all the elements are bing rendered
describe('create-category component', () => {
  
  // create a new component
  const createComp = () =>
    shallow(
      <CreateCategory
        onCategoryChanged   = {event       => {}}
        onSubmit            = {event       =>{}}
      />
    );

    //test to see if components exist
    it('renders login', () => {
      const component = createComp();

      expect(component.find("div")      .length).toBe(3);
      expect(component.find("input")    .length).toBe(1);
      expect(component.find("h1")       .length).toBe(1);
      expect(component.find("button")   .length).toBe(1);
    });
        
});