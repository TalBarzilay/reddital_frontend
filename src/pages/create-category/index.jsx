import React, { useReducer, useCallback } from 'react';
import CreateCategory from './create-category.component';
import reducer from './create-category.reducer';
import {CATEGORY_CHANGED } from './create-category.actions';

import { useSelector } from 'react-redux';
import {selectCategoryService, selectMessageService, selectAuthenticationService } from '../../redux/service/service.selectors';

import { useNavigate, Navigate } from 'react-router-dom';


const CreateCategoryIndex = () => {

  // ------------------------------------------- state -------------------------------------------
 
  
  const [state, dispatch] = useReducer(reducer, reducer());

  const navigate = useNavigate();

  const categoryService         = useSelector(selectCategoryService);
  const messageService          = useSelector(selectMessageService);
  const authenticationService   = useSelector(selectAuthenticationService);

  // ------------------------------------------- action handlers -------------------------------------------
 

  /**
   * handle pressing the login vakue
   * @param {*} state the current state when clickin the buttom
   */
  const handleCreate = useCallback(async category => {
    if(category !== ""){
      //inputs are valid, contacting the server....

      try{
        await categoryService.createCategory(category);
        messageService.success('created category in successfully');
        
        navigate('/subredditals');
        
      } catch(err) {
        messageService.err(err);
      }
    } else {
      messageService.error('category is invalid', 'please provide a valid category');
    }
  }, [messageService, categoryService, navigate]);


  // ------------------------------------------- return statement -------------------------------------------
 
  return !authenticationService.isAdmin() ?
    (<Navigate to='/subredditals'/>) : (
        <CreateCategory
            {...state}

            onCategoryChanged  = {event      => dispatch(CATEGORY_CHANGED(event.target.value))}

            onSubmit           = {()       => handleCreate(state.category)}
        />);
};







export default CreateCategoryIndex;
