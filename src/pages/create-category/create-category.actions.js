import  actions from './actionEnum';

export const CATEGORY_CHANGED = category => 
    ({type: actions.CATEGORY_CHANGED , payload:category});