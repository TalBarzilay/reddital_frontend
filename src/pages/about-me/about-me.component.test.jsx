import React from 'react';
import { shallow } from 'enzyme';

import AboutMe from './about-me.component'

describe('about-me component', () => {

    const createComponent = () =>
    shallow(
      <AboutMe 
      />
    );

    // test that reducer returns currect inital state without arguments
    it('render about-me', () => {
        const aboutMe = createComponent();

        expect(aboutMe.find("div").length).toBe(1);
        expect(aboutMe.find("h3").length).toBe(7);
    });

});