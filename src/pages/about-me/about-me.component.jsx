import React from 'react'
import styles from './about-me.module.css';

export default () => {
    return (
        <div className={`${styles.aboutContainer}`}>
            <h3 className={`${styles.innerItem} lobster-font`}>@Created by Tal Barzialy</h3>
            
            <h3 className={`${styles.innerItem} sil-font`}>This project was made during November 21 - Januarry 22 in about 2.5 months</h3>
            <h3 className={`${styles.innerItem} sil-font`}>Technological stack : React, spring-boot, hibernate, postgreSQL</h3>
            <br />
            <h3 className={`${styles.innerItem} sil-font`}>Tools : </h3>
            
            <h3 className={`${styles.innerItem} sil-font`}>Back  : lombok, junit5 </h3> 
            <h3 className={`${styles.innerItem} sil-font`}>Front : redux, axios, router, ensyme, jest </h3> 
            <h3 className={`${styles.innerItem} sil-font`}>Both  : Heroku, GitLab, docker-compose, JWT, CI-CD, developed in TDD methodology</h3>                  
        </div>
    )
}
