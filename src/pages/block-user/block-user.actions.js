import  actions from './actionEnum';

export const USERNAME_CHANGED = username => 
    ({type: actions.USERNAME_CHANGED , payload:username});