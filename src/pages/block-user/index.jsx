import React, { useReducer, useCallback } from 'react';
import BlockUser from './block-user.component';
import reducer from './block-user.reducer';
import {USERNAME_CHANGED } from './block-user.actions';

import { useSelector } from 'react-redux';
import { selectMessageService, selectAuthenticationService } from '../../redux/service/service.selectors';

import { useNavigate, Navigate } from 'react-router-dom';


const BlockUserIndex = () => {

  // ------------------------------------------- state -------------------------------------------
 
  
  const [state, dispatch] = useReducer(reducer, reducer());

  const navigate = useNavigate();

  const messageService          = useSelector(selectMessageService);
  const authenticationService   = useSelector(selectAuthenticationService);

  // ------------------------------------------- action handlers -------------------------------------------
 

  /**
   * handle pressing the block bottun, and permenetly ban the user
   * @param {*} state the current state when clickin the buttom
   */
  const handleBlock = useCallback(async username => {
    if(username !== ""){
      //inputs are valid, contacting the server....

      try{
         const response = await authenticationService.block(username);

        if(response.data.blocked){
            messageService.success('user has been blocked!');
        } else {
            messageService.success('user has been unblocked!');
        }
        
        navigate('/adminHub');
        
      } catch(err) {
        messageService.err(err);
      }
    } else {
      messageService.error('username is invalid', 'please provide a valid username');
    }
  }, [messageService,authenticationService, navigate]);


  // ------------------------------------------- return statement -------------------------------------------
 
  return !authenticationService.isAdmin() ?
    (<Navigate to='/subredditals'/>) : (
        <BlockUser
            {...state}

            onUsernameChanged   = {event      => dispatch(USERNAME_CHANGED(event.target.value))}

            onBlock             = {()       => handleBlock(state.username)}
        />);
};







export default BlockUserIndex;
