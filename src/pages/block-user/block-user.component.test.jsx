import React from 'react';
import { shallow } from 'enzyme';

import BlockUser from './block-user.component';



//test to see that all the elements are bing rendered
describe('cblock-user component', () => {
  
  // create a new component
  const createComp = () =>
    shallow(
      <BlockUser
        onUsernameChanged   = {event       => {}}
        onBlock             = {event       =>{}}
      />
    );

    //test to see if components exist
    it('renders component', () => {
      const component = createComp();

      expect(component.find("div")      .length).toBe(3);
      expect(component.find("input")    .length).toBe(1);
      expect(component.find("h1")       .length).toBe(1);
      expect(component.find("button")   .length).toBe(1);
    });
        
});