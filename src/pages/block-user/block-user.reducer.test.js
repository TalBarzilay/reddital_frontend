import reducer from './block-user.reducer';
import {USERNAME_CHANGED} from './block-user.actions';


// test reducer function
describe('block-user reducer', () => {

    // test that reducer returns currect inital state without arguments
    it('reduce no argument', () => {
        expect(reducer()) .toStrictEqual({username:""});
    });

    //test reducer's USERNAME_CHANGE action
    it('reduce USERNAME_CHANGED', () => {
      const username = "BananaKing69";

      expect(reducer(undefined        , USERNAME_CHANGED(username)))  .toStrictEqual({username});
      expect(reducer({username:""}    , USERNAME_CHANGED(username)))  .toStrictEqual({username});
      expect(reducer({username:"old"} , USERNAME_CHANGED(username)))  .toStrictEqual({username});
  });
});