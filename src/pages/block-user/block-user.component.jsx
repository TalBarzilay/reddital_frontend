import React from 'react';
import styles from './block-user.module.css';

const BlockUser = ({onUsernameChanged, onBlock}) => {

    return (
        <div className= {`${styles.container}`}>
            <div className= {`${styles.blockBlock} form-block`}>
           
                <h1 className={`${styles.title} lobster-font`}>Ban User</ h1>

                <div className= {`${styles.textButtonContainer}`}>
                    <input className={`input ${styles.textinput}`} onChange={onUsernameChanged} placeholder="username" type="text"></input> 
                    <button className={`${styles.blockButon} buton sil-font`} onClick={onBlock} > toggle Ban</button>
                </div>
            </div>
        </div>
    )
};

export default BlockUser;