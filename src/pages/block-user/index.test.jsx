import React from 'react';
import { mount } from 'enzyme';
import {BrowserRouter as Router} from 'react-router-dom';

import {Provider} from 'react-redux';
import store from '../../redux/store'; 

import BlockUser from './index';


jest.mock('../../services/authentication.service', () => ({
    getInstance: () => ({
        isAdmin    : () => true,
    })  
}));

jest.mock('../../services/message.service', () => jest.fn().mockImplementation(() => ({
    info            : jest.fn(),
    success         : jest.fn(),
    warn            : jest.fn(),
    error           : jest.fn(),
    err             : jest.fn(),
    addNotification : jest.fn(),
  })));
  

jest.mock('../../services/web-request.service', () => jest.fn());





//test to see that all the elements are bing rendered
describe('block-user index', () => {

    // create a new component
    const createIndex = () =>
        mount(<Provider store={store}><Router><BlockUser/></Router></Provider>);

    const getComponent = wrapper => wrapper.find("BlockUser");

    it('username changed', () => {
        let wrper     = createIndex();
        let create    = getComponent(wrper);
        let textbox   = create.find('input').at(0);

        expect(textbox.length).toEqual(1);
       
       
        let value = "mrBanana";

        // valid text
        textbox.simulate("change", { target: { value } });
        wrper.update();
        create   = getComponent(wrper);
        expect(create.prop("username")).toBe(value);

        //empty string 
        value = ""
        textbox.simulate("change", { target: { value} });
        wrper.update();
        create   = getComponent(wrper);
        expect(create.prop("username")).toBe(value);
    });

});