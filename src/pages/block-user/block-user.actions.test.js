import  actions from './actionEnum';
import {USERNAME_CHANGED} from './block-user.actions';

describe('block-user actions', () => {

    it('USERNAME_CHANGED', () => {
        const username = "mrBanana";

      expect(USERNAME_CHANGED(username)).toStrictEqual({type: actions.USERNAME_CHANGED , payload:username});
    });
  });