import React from 'react';
import { mount } from 'enzyme';
import {BrowserRouter as Router} from 'react-router-dom';

import {Provider} from 'react-redux';
import store from '../../redux/store'; 

import Posting from './index';


jest.mock('../../services/authentication.service', () => ({
    getInstance: () => ({
        getLoggedUser    : () => "banana",
    })  
}));

jest.mock('react-router', () => ({
    ...jest.requireActual('react-router'),
    useMatch: () => ({
      params: {subreddital:42}
    })
  }));

jest.mock('../../services/message.service', () => jest.fn().mockImplementation(() => ({
    info            : jest.fn(),
    success         : jest.fn(),
    warn            : jest.fn(),
    error           : jest.fn(),
    err             : jest.fn(),
    addNotification : jest.fn(),
  })));
  


jest.mock('../../services/post.service', () => jest.fn());
jest.mock('../../services/web-request.service', () => jest.fn());




//test to see that all the elements are bing rendered
describe('posting index', () => {

    // create a new component
    const createPostingIndex = () =>
        mount(<Provider store={store}><Router><Posting/></Router></Provider>);

    // test title changed
    it('subreddit changed', () => {
        let wrper     = createPostingIndex();
        let posting   = wrper.find("Posting");
        let textbox   = posting.find('#title');

        expect(textbox.length).toEqual(1);

       
       
        let value = "hello!";

        // valid text
        textbox.simulate("change", { target: { value } });
        wrper.update();
        posting   = wrper.find("Posting");
        expect(posting.prop("title")).toBe(value);

        //empty string 
        value = ""
        textbox.simulate("change", { target: { value} });
        wrper.update();
        posting   = wrper.find("Posting");
        expect(posting.prop("title")).toBe(value);
    });

  // title changed
  it('post text changed', () => {
    let wrper     = createPostingIndex();
    let posting   = wrper.find("Posting");
    let textbox   = posting.find('#post-text');

    expect(textbox.length).toEqual(1);

   
   
    let value = "my name is yosi! hello!";

    // valid text
    textbox.simulate("change", { target: { value } });
    wrper.update();
    posting   = wrper.find("Posting");
    expect(posting.prop("post")).toBe(value);

    //empty string 
    value = ""
    textbox.simulate("change", { target: { value} });
    wrper.update();
    posting   = wrper.find("Posting");
    expect(posting.prop("post")).toBe(value);
});

});