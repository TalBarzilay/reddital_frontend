import React, { useReducer, useCallback } from 'react';
import Posting from './posting.component';
import reducer from './posting.reducer';
import getPayload from './posting.actions';
import { useMatch, useNavigate, Navigate } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import {selectMessageService, selectPostService, selectAuthenticationService} from '../../redux/service/service.selectors';
import {ADD_POST} from '../../redux/todays-activities/todays-activities.actions';

const PostingIndex = () => {

  // ------------------------------------------- state -------------------------------------------
  
  const [state, dispatch] = useReducer(reducer, reducer());
  const match = useMatch('/:subreddital/post');
  const navigate = useNavigate();

  const messageService        = useSelector(state => selectMessageService(state));
  const postService           = useSelector(state => selectPostService(state));
  const authenticationService = useSelector(state => selectAuthenticationService(state));

  const udispatch = useDispatch();

  // ------------------------------------------- action handlers -------------------------------------------

  /**
   * handle pressing the login vakue
   * @param {*} state the current state when clickin the buttom
   */
  const handlePosting = useCallback(async state => {

    if(state.title !== ""){
      //inputs are valid, contacting the server...

      try {
        const respone =  await postService.post(state.title, state.post, match.params.subreddital);
        
        udispatch(ADD_POST(respone.data)); //ADD post to today's activities state
        
        messageService.success('posted successfully!');
        navigate(`/${match.params.subreddital}/posts`);

      } catch(err) {
        messageService.err(err);
      }
    } else {
      messageService.error('title is missing', 'please provide valid content.'); // the inputs atre not valid
    }
  }, [postService, messageService, navigate, udispatch, match.params.subreddital]);

  // ------------------------------------------- return statement -------------------------------------------
 
  return authenticationService.getLoggedUser() == null ?
    (<Navigate to='/login'/>) : (
  <Posting
     {...state}

     onTitleChange  = {event      => dispatch(getPayload().TITLE_CHANGE(event.target.value))}
     onPostChange  = {event       => dispatch(getPayload().POST_CHANGE(event.target.value))}

     onPost           = {()       => handlePosting(state)}
  />);
};






export default  PostingIndex;
