import  reducer from './posting.reducer';
import getPayload from './posting.actions';


// test reducer function
describe('posting reducer', () => {

    // test that reducer returns currect inital state without arguments
    it('reduce no argument', () => {
        expect(reducer()) .toStrictEqual({post:"", title:""});
    });

    //test reducer's TITLE_CHANGE action
    it('reduce TITLE_CHANGE', () => {
        const title = "if the elections for the USA would happand today, who do you think would win?";

        expect(reducer(undefined                                                  , getPayload().TITLE_CHANGE(title)))    .toStrictEqual({post:"", title:title});
        expect(reducer({post:"", title:"old"}                                     , getPayload().TITLE_CHANGE(title)))    .toStrictEqual({post:"", title:title});
        expect(reducer({post:"I think Spongebob SquarePants would!", title:"old"} , getPayload().TITLE_CHANGE(title)))    .toStrictEqual({post:"I think Spongebob SquarePants would!", title:title});
        expect(reducer({post:"", title:"old"}                                     , getPayload().TITLE_CHANGE("")))       .toStrictEqual({post:"", title:""});
    });

    //test reducer's TITLE_CHANGE action
    it('reduce POST_CHANGE', () => {
        const post = "do you preffer pancakes or broccoli?";
    
        expect(reducer(undefined                                        , getPayload().POST_CHANGE(post)))  .toStrictEqual({post:post, title:""});
        expect(reducer({post:"", title:""}                              , getPayload().POST_CHANGE(post)))  .toStrictEqual({post:post, title:""});
        expect(reducer({post:"old", title:"what is your perfect food?"} , getPayload().POST_CHANGE(post)))  .toStrictEqual({post:post, title:"what is your perfect food?"});
        expect(reducer({post:"", title:"old"}                           , getPayload().POST_CHANGE("")))    .toStrictEqual({post:"", title:"old"});
    });
});