import React from 'react'

import styles from './posting.module.css';

export default function Posting({onPostChange, onTitleChange, onPost}) {
    return (
        <div className={styles.container}>
            <div className={`${styles.posting} form-block`}>
                <h2 className={`${styles.post_title} lobster-font`}>Post</ h2>

                <input id="title" className={`input ${styles.textinput2}`} onChange={onTitleChange} placeholder="title" type="text"></input> 
                <br />

                <textarea id="post-text" className={`input ${styles.textarea}`} onChange={onPostChange} placeholder="create new post now!" type="text"></textarea> 
                <br />
                <br />

                <button id="post" className={`${styles.postButon} buton sil-font`} onClick={onPost} type="submit">Post</button>       
            </div>
        </div>
    )
}
