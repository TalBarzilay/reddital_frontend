import actions from './actionEnum';

/**
 * @returns the payload to send for the reducer function, depending on the type of action
 */
 const getPayload = () => ({
    TITLE_CHANGE      : newVal  =>  ({type: actions.TITLE_CHANGE      , payload: newVal}),
    POST_CHANGE       : newVal  =>  ({type: actions.POST_CHANGE       , payload: newVal}),
});


export default getPayload;