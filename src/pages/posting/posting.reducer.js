import actions from './actionEnum';

/** The inital state of the reducer, will return by callig reducer with no arguments */
const INITIAL_STATE = {post:"", title:""};


// ------------------------------------------- exported methods -------------------------------------------


/**
 * the posting reducer
 * @param {*} state the current state
 * @param {*} param1 the action to perform, that was recived from getPayload.
 * @returns the new state
 */
const reducer = ( state=INITIAL_STATE, {type, payload} = {}) => {
    switch (type) {

      case actions.TITLE_CHANGE:      // title has changed
        return {...state, title: payload};

      case actions.POST_CHANGE:       // post has changed
        return {...state, post: payload};


      default: 
        return state;
    }
  };
  


export default reducer;