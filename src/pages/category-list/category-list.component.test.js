import React from 'react';
import { shallow } from 'enzyme';

import CategoryList from './category-list.component';

describe('category-list component', () => {

    const createComponent = () =>
    shallow(
      <CategoryList
        categories = {[]}  
      />
    );

    // test that reducer returns currect inital state without arguments
    it('render category-list', () => {
        const catlist = createComponent();

        expect(catlist.find("div").length).toBe(1);
    });

});