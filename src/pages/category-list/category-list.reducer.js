import actions from './actionEnum';

/** The inital state of the reducer, will return by callig reducer with no arguments */
const INITIAL_STATE = {categories:[]};






/**
 * the post list reducer
 * @param {*} state the current state
 * @param {*} param1 the action to perform, that was recived from getPayload.
 * @returns the new state
 */
 const reducer = ( state=INITIAL_STATE, {type, payload} = {}) => {
    switch (type) {

        case actions.INIT_CATEGORIES:
            return {...state, categories: payload};

        default: 
            return state;
    }
  };



export default reducer;