import reducer from './category-list.reducer';
import getPayload from './category-list.actions';

// test reducer function
describe('category-list reducer', () => {

    // test that reducer returns currect inital state without arguments
    it('reduce no argument', () => {
        expect(reducer()) .toStrictEqual({categories:[]});
    });

    //test reducer's INIT_CATEGORIES action
    it('reduce INIT_CATEGORIES', () => {
        const categories = [
            {
                "id": 1,
                "name": "smalltalks",
                "subredditals": [
                    {
                        "id": 2,
                        "name": "askTal2",
                        "category": "smalltalks"
                    },
                    {
                        "id": 3,
                        "name": "askTal",
                        "category": "smalltalks"
                    }
                ]
            },
            {
                "id": 4,
                "name": "gaming",
                "subredditals": [
                    {
                        "id": 5,
                        "name": "subnautica",
                        "category": "gaming"
                    },
                    {
                        "id": 6,
                        "name": "terreria",
                        "category": "gaming"
                    }
                ]
            }
        ];

      expect(reducer(undefined , getPayload().INIT_CATEGORIES(categories)))  .toStrictEqual({categories});
      expect(reducer({categories:[]} , getPayload().INIT_CATEGORIES(categories)))  .toStrictEqual({categories});
  });

});