import actions from './actionEnum';


/**
 * @returns the payload to send for the reducer function, depending on the type of action
 */
const getPayload = () => ({
    INIT_CATEGORIES   : categories  =>  ({type: actions.INIT_CATEGORIES , payload: categories}),
});


export default getPayload;