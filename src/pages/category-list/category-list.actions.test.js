import  actions from './actionEnum';
import getPayload from './category-list.actions'

//test getPayload function
describe('catrgory-list getPayload', () => {

    it('getPayload', () => {
        const categories = [
            {
                "id": 1,
                "name": "smalltalks",
                "subredditals": [
                    {
                        "id": 2,
                        "name": "askTal2",
                        "category": "smalltalks"
                    },
                    {
                        "id": 3,
                        "name": "askTal",
                        "category": "smalltalks"
                    }
                ]
            },
            {
                "id": 4,
                "name": "gaming",
                "subredditals": [
                    {
                        "id": 5,
                        "name": "subnautica",
                        "category": "gaming"
                    },
                    {
                        "id": 6,
                        "name": "terreria",
                        "category": "gaming"
                    }
                ]
            }
        ];
  
      expect(getPayload().INIT_CATEGORIES(categories)).toStrictEqual({type: actions.INIT_CATEGORIES , payload:categories});
    });
  });