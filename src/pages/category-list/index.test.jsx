import React from 'react';
import { shallow } from 'enzyme';

import {Provider} from 'react-redux';
import store from '../../redux/store'; 


import CategoryList from './index';



describe('category-list index', () => {

    // create a new component
    const createIndex = () =>
        shallow(<Provider store = {store}><CategoryList/> </Provider>);

    
    beforeEach(() => {
        jest.spyOn(React, "useEffect").mockImplementationOnce(cb => cb());     
    });

    // test that reducer returns currect inital state without arguments
    it('test index', () => {
        const wrapper = createIndex();

        expect(wrapper.length).toBe(1)
    });

});