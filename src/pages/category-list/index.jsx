import React, { useReducer, useEffect, useCallback } from 'react';
import CategoryList from './category-list.component';

import { useSelector } from 'react-redux';
import { selectCategoryService, selectMessageService } from '../../redux/service/service.selectors';

import reducer from './category-list.reducer';
import getPayload from './category-list.actions';


const CategortListIndex = () => {

    // ------------------------------------------- state -------------------------------------------
  
    const [state, dispatch] = useReducer(reducer, reducer());

    const messageService    = useSelector(selectMessageService);
    const categoryService   = useSelector(selectCategoryService);


    // ------------------------------------------- methods -------------------------------------------

    const initState = useCallback(async () => {
        try{
            const response = await categoryService.subredditals();
            dispatch(getPayload().INIT_CATEGORIES(response.data));
        } catch(err) {
            messageService.err(err);
        }
    }, [categoryService, messageService]);
    

    // ------------------------------------------- init with posts -------------------------------------------

    useEffect(() => {
        initState();
    }, [initState]);
    

    // ------------------------------------------- return statement -------------------------------------------

    return (
        <CategoryList 
            {...state}
        />
    )
}






export default CategortListIndex;
