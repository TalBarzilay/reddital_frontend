import React from 'react';
import styles from './category-list.module.css';

import SubCategoryList from '../../components/sub-category-list';

export default function CategoryList({categories}) {
    return (
        <div className={`${styles.CategoriesContainer}`}>
             {categories.map(category => <SubCategoryList key={category.id} {...category} />)}
        </div>
    )
}
