import actions from './actionEnum';

/** The inital state of the reducer, will return by callig reducer with no arguments */
const INITIAL_STATE = {username:"",email:"",password:"" , repeatPassword:"", userErr:" ",emailErr:" ",passwordErr:" ", repeatPasswordErr:" "};

// ------------------------------------------- validators -------------------------------------------

  /**
   * @returns an error mesage for a the username, empty string if valid
   */
   const validateUsername = username => {
    if (username === "") {
      return "* please provide a username";
    }

    return "";
  };

   /**
   * @returns an error mesage for a the email, empty string if valid
   */
  const validateEmail = email => {
    if (email=== "") {
      return "* please provide an email"
    }

    return "";
  };

   /**
   * @returns an error mesage for a the password, empty string if valid
   */
  const validatePassword = password => {
    if (password=== "") {
      return "* please provide a password"
    }
    
    if (password.length < 6) {
      return "* please provide a password of length 6 atleast"
    }

    return "";
  };


  const validateRepeatPassword = (password, repeatPassword) => {
    if (password !== repeatPassword) {
      return "* passwords do not match"
    }
    
    return "";
  };



// ------------------------------------------- reducer methods -------------------------------------------


/**
 * the register reducer
 * @param {*} state the current state
 * @param {*} param1 the action to perform, that was recived from getPayload.
 * @returns 
 */
const reducer = ( state=INITIAL_STATE, {type, payload} = {}) => {

    switch (type) {

      case actions.USERNAME_CHANGE: // username has changed
        return {...state, username: payload, userErr: validateUsername(payload)};

      case actions.EMAIL_CHANGE:  // email has changed
        return {...state, email: payload, emailErr:validateEmail(payload)};

      case actions.PASSWORD_CHANGE:  // password has changed
        return {...state, password: payload, passwordErr:validatePassword(payload), repeatPasswordErr:validateRepeatPassword(payload, state.repeatPassword)};

      case actions.REPEAT_PASSWORD_CHANGE:  // repeated password has changed
        return {...state, repeatPassword: payload, repeatPasswordErr:validateRepeatPassword(state.password, payload)};

      default: 
        return state;
    }
  };
  


export default  reducer;