import React, { useReducer, useCallback } from 'react';
import Register from './register.component';
import reducer from './register.reducer';
import getPayload from './register.actions';

import { useSelector } from 'react-redux';
import {selectMessageService, selectAuthenticationService} from '../../redux/service/service.selectors';

import { useNavigate, Navigate } from 'react-router-dom';

const RegisterIndex =  () => {

  // ------------------------------------------- state -------------------------------------------
  
  const [state, dispatch] = useReducer(reducer, reducer());
  const navigate = useNavigate();

  const messageService        = useSelector(state => selectMessageService(state));
  const authenticationService = useSelector(state => selectAuthenticationService(state));

  // ------------------------------------------- handlers -------------------------------------------

  /**
   * handle the register button click
   * @param {*} state the cuurent state
   */
  const registerHandler = useCallback(async state => {

    if(state.userErr === "" && state.emailErr === "" && state.passwordErr === "" && state.repeatPasswordErr === ""){
      //inputs are valid, contacting the server...

      try {
        await authenticationService.signup(state.username, state.email, state.password);
        
        messageService.success('SUCCESS' ,'registered successfully');

        navigate('/login');
      } catch(err){
        messageService.err(err)
      }
      
    } else {
      messageService.error('bad info!', 'please provide valid credentials'); //inputs are not valid
    }
  }, [authenticationService, messageService, navigate]);

  // ------------------------------------------- return statement -------------------------------------------
 
  return authenticationService.getLoggedUser() != null ?
    (<Navigate to='/subredditals'/>) :

   (<Register
    {...state}

    onUserNameChange      = {event      =>  dispatch(getPayload().USERNAME_CHANGE(event.target.value))}
    onEmailChange         = {event      =>  dispatch(getPayload().EMAIL_CHANGE(event.target.value))}
    onPasswordChange      = {event      =>  dispatch(getPayload().PASSWORD_CHANGE(event.target.value))}
    onRepeatPasswordChange= {event      =>  dispatch(getPayload().REPEAT_PASSWORD_CHANGE(event.target.value))}

    onRegister        = {()         =>  registerHandler(state)}
    onLoginRedirect   = {()         =>  navigate('/login')}
  />);
};





export default RegisterIndex;
