import React from 'react'
import styles from './register.module.css'

export default ({onUserNameChange, onEmailChange, onPasswordChange, onRepeatPasswordChange, onLoginRedirect, onRegister, userErr, emailErr, passwordErr, repeatPasswordErr}) => {

    return (
        <div className= {`${styles.Signin} center form-block`}>
           
            <h1 className={`${styles.title} lobster-font`}>Register</ h1>

            <input id="username" className={`input ${styles.textinput}`} onChange={onUserNameChange} placeholder="username" type="text"></input> 
            <br />
            <label id="username-validator" className={`validator`}>{userErr}</label>

            <br />

            <input id="email" className={`input ${styles.textinput}`} onChange={onEmailChange} placeholder="email" type="email"></input> 
            <br />
            <label id="email-validator" className={`validator`}>{emailErr}</label>

            <br />

            <input id="password" className={`input ${styles.textinput}`} onChange={onPasswordChange} placeholder="password" type="password"></input> 
            <br />
            <label id="password-validator" className={`validator`}>{passwordErr}</label>

            <br />

            <input id="repassword" className={`input ${styles.textinput}`} onChange={onRepeatPasswordChange} placeholder="repeat password" type="password"></input> 
            <br />
            <label id="repassword-validator" className={`validator`}>{repeatPasswordErr}</label>

            <br />
            <br />
            <button id="register" className={`buton ${styles.registerButon} sil-font`} onClick={onRegister} type="submit">Register</button>

            <button id="loginRedirect" onClick={onLoginRedirect} className={`${styles.redirectlogin} link`}>already have one? login now!</button>
        </div>
    )
}