import reducer from './register.reducer';
import getPayload from './register.actions';

// test reducer function
describe('register reducer', () => {

  // test that reducer returns currect inital state without arguments
  it('reduce no argument', () => {
      expect(reducer()) .toStrictEqual({username:"",email:"", password:"", repeatPassword:"" , userErr:" ",emailErr:" ", passwordErr:" ", repeatPasswordErr:" "});
  });

  //test reducer's USERNAME_CHANGE action
  it('reduce USERNAME_CHANGE', () => {
    const username = "banana";

    expect(reducer(undefined                                                                                                                                                     , getPayload().USERNAME_CHANGE(username)))  .toStrictEqual({username,email:"",password:"", repeatPassword:"" , userErr:"",emailErr:" ",passwordErr:" ", repeatPasswordErr:" "});
    expect(reducer({username:"", email:"", password:"123" ,repeatPassword:"" , userErr:"", emailErr:" ", passwordErr:"tooshort", repeatPasswordErr:"* passwords do not match"}   , getPayload().USERNAME_CHANGE(username)))  .toStrictEqual({username,email:"",password:"123", repeatPassword:"" , userErr:"",emailErr:" ",passwordErr:"tooshort", repeatPasswordErr:"* passwords do not match"});
    expect(reducer({username:"old", email:"", password:"123" , repeatPassword:"" ,userErr:"", emailErr:" ", passwordErr:"tooshort", repeatPasswordErr:"* passwords do not match"}, getPayload().USERNAME_CHANGE(username)))  .toStrictEqual({username,email:"",password:"123", repeatPassword:"" , userErr:"",emailErr:" ",passwordErr:"tooshort", repeatPasswordErr:"* passwords do not match"});
    expect(reducer({username:"", email:"", password:"123" ,repeatPassword:"" , userErr:"", emailErr:" ", passwordErr:"tooshort", repeatPasswordErr:"* passwords do not match"}   , getPayload().USERNAME_CHANGE("")))        .toStrictEqual({username:"",email:"",password:"123", repeatPassword:"" , userErr:"* please provide a username",emailErr:" ",passwordErr:"tooshort", repeatPasswordErr:"* passwords do not match"});
  });

  //test reducer's EMAIL_CHANGE action
  it('reduce EMAIL_CHANGE', () => {
    const email = "tal@barzialy.yosi";

    expect(reducer(undefined                                                                                                                                                     , getPayload().EMAIL_CHANGE(email)))  .toStrictEqual({username:"", email, password:"", repeatPassword:"" , userErr:" ", emailErr:"", passwordErr:" ", repeatPasswordErr:" "});
    expect(reducer({username:"yosiking", email:"", password:"" ,repeatPassword:"" , userErr:"", emailErr:" ", passwordErr:" ", repeatPasswordErr:"* passwords do not match"}     , getPayload().EMAIL_CHANGE(email)))  .toStrictEqual({username:"yosiking", email, password:"", repeatPassword:"" , userErr:"", emailErr:"", passwordErr:" ", repeatPasswordErr:"* passwords do not match"});
    expect(reducer({username:"yosiking", email:"old", password:"" ,repeatPassword:"" , userErr:"", emailErr:" ", passwordErr:" ", repeatPasswordErr:"* passwords do not match"}  , getPayload().EMAIL_CHANGE(email)))  .toStrictEqual({username:"yosiking", email, password:"", repeatPassword:"" , userErr:"", emailErr:"", passwordErr:" ", repeatPasswordErr:"* passwords do not match"});
    expect(reducer({username:"yosiking", email:"a@a.a", password:"" ,repeatPassword:"" , userErr:"",emailErr:"", passwordErr:" ", repeatPasswordErr:"* passwords do not match"}  , getPayload().EMAIL_CHANGE("")))     .toStrictEqual({username:"yosiking", email:"", password:"", repeatPassword:"" , userErr:"", emailErr:"* please provide an email", passwordErr:" ", repeatPasswordErr:"* passwords do not match"});
  });

  //test reducer's PASSWORD_CHANGE action
  it('reduce PASSWORD_CHANGE', () => {
    const password = "12345678";

    expect(reducer(undefined                                                                                                                                                           , getPayload().PASSWORD_CHANGE(password)))  .toStrictEqual({username:"", email:"", password , repeatPassword:"" , userErr:" ", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"});
    expect(reducer({username:"yosiking", email:"", password:"" ,repeatPassword:"" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"}            , getPayload().PASSWORD_CHANGE(password)))  .toStrictEqual({username:"yosiking", email:"", password , repeatPassword:"" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"});
    expect(reducer({username:"yosiking", email:"", password:"old" ,repeatPassword:"" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"}         , getPayload().PASSWORD_CHANGE(password)))  .toStrictEqual({username:"yosiking", email:"", password , repeatPassword:"" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"});
    expect(reducer({username:"yosiking", email:"", password:"123" ,repeatPassword:"" , userErr:"",emailErr:" ", passwordErr:"tooshort", repeatPasswordErr:"* passwords do not match"}  , getPayload().PASSWORD_CHANGE("")))        .toStrictEqual({username:"yosiking", email:"", password:"" , repeatPassword:"" , userErr:"", emailErr:" ", passwordErr:"* please provide a password", repeatPasswordErr:""});
  });

  //test reducer's PASSWORD_CHANGE action
  it('reduce REPEAT_PASSWORD_CHANGE', () => {
    const password = "12345678";

    expect(reducer(undefined                                                                                                                                                           , getPayload().REPEAT_PASSWORD_CHANGE(password)))  .toStrictEqual({username:"", email:"", password:"" , repeatPassword:password , userErr:" ", emailErr:" ", passwordErr:" ", repeatPasswordErr:"* passwords do not match"});
    expect(reducer({username:"yosiking", email:"", password:"" ,repeatPassword:"" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"}            , getPayload().REPEAT_PASSWORD_CHANGE(password)))  .toStrictEqual({username:"yosiking", email:"", password:"" , repeatPassword:password , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"});
    expect(reducer({username:"yosiking", email:"", password:"old" ,repeatPassword:"old" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"}         , getPayload().REPEAT_PASSWORD_CHANGE(password)))  .toStrictEqual({username:"yosiking", email:"", password:"old" , repeatPassword:password , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"});
    expect(reducer({username:"yosiking", email:"", password:"123" ,repeatPassword:"123" , userErr:"",emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"}  , getPayload().REPEAT_PASSWORD_CHANGE("")))        .toStrictEqual({username:"yosiking", email:"", password:"123" , repeatPassword:"" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"});
  });


  //test reducer's PASSWORD_CHANGE action
  it('marching password test', () => {

    expect(reducer({username:"", email:"", password:"1234" ,repeatPassword:"12" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"}            , getPayload().REPEAT_PASSWORD_CHANGE("123")))  .toStrictEqual({username:"", email:"", password:"1234" ,repeatPassword:"123" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"} );
    expect(reducer({username:"", email:"", password:"1234" ,repeatPassword:"123" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"}            , getPayload().REPEAT_PASSWORD_CHANGE("1234")))  .toStrictEqual({username:"", email:"", password:"1234" ,repeatPassword:"1234" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:""} );

    expect(reducer({username:"", email:"", password:"1234" ,repeatPassword:"1234" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"}            , getPayload().PASSWORD_CHANGE("12345")))  .toStrictEqual({username:"", email:"", password:"12345" ,repeatPassword:"1234" , userErr:"", emailErr:" ", passwordErr:"* please provide a password of length 6 atleast", repeatPasswordErr:"* passwords do not match"} );
    expect(reducer({username:"", email:"", password:"12345" ,repeatPassword:"1234" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"}            , getPayload().PASSWORD_CHANGE("123456")))  .toStrictEqual({username:"", email:"", password:"123456" ,repeatPassword:"1234" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"} );
    expect(reducer({username:"", email:"", password:"123456" ,repeatPassword:"1234" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:"* passwords do not match"}            , getPayload().REPEAT_PASSWORD_CHANGE("123456")))  .toStrictEqual({username:"", email:"", password:"123456" ,repeatPassword:"123456" , userErr:"", emailErr:" ", passwordErr:"", repeatPasswordErr:""} );

  });


});