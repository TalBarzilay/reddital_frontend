const actions = {
    USERNAME_CHANGE : 'unc',
    EMAIL_CHANGE : 'ec',
    PASSWORD_CHANGE : 'pc',
    REPEAT_PASSWORD_CHANGE : 'rpc'
};

export default actions;