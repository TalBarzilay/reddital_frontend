import actions from './actionEnum';
import getPayload from './register.actions';

//test getPayload function
describe('register getPayload', () => {

    it('getPayload', () => {
      const username = "banana";
      const email = "tal@barzialy.yosi";
      const password = "12345678";
  
      expect(getPayload().USERNAME_CHANGE(username)).toStrictEqual({type: actions.USERNAME_CHANGE , payload: username});
      expect(getPayload().EMAIL_CHANGE(email))      .toStrictEqual({type: actions.EMAIL_CHANGE    , payload: email});
      expect(getPayload().PASSWORD_CHANGE(password)).toStrictEqual({type: actions.PASSWORD_CHANGE , payload: password});
      expect(getPayload().REPEAT_PASSWORD_CHANGE(password)).toStrictEqual({type: actions.REPEAT_PASSWORD_CHANGE , payload: password});
    });
  });