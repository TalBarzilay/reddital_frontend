import reducer from './bi.reducer';
import {INIT_INFO} from './bi.actions';

// test reducer function
describe('bi reducer', () => {

    // test that reducer returns currect inital state without arguments
    it('reduce no argument', () => {
        expect(reducer()) .toStrictEqual({info:{}});
    });

    //test reducer's INIT_INFO action
    it('reduce INIT_INFO', () => {

        const info = {numOfUsers: 69, numOfPosts:42};

        expect(reducer(undefined , INIT_INFO(info)))  .toStrictEqual({info});
        expect(reducer({info:{}} , INIT_INFO(info)))  .toStrictEqual({info});
  });

});