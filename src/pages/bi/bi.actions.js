import actions from './actionEnum';


/**
 * @returns the payload to send for the reducer function, depending on the type of action
 */
export const  INIT_INFO   = info  =>  
    ({type: actions.INIT_INFO , payload: info});

