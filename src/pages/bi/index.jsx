import React, {useReducer, useEffect, useCallback} from 'react';
import Bi from './bi.component';
import reducer from './bi.reducer';
import {INIT_INFO} from './bi.actions';

import { useSelector } from 'react-redux';

import { Navigate } from 'react-router-dom';

import { selectAuthenticationService, selectBiService, selectMessageService } from '../../redux/service/service.selectors';

export default () => {

    // ------------------------------------------- state -------------------------------------------

    const [state, dispatch] = useReducer(reducer, reducer());
    
    const messageService    = useSelector(selectMessageService);
    const biService         = useSelector(selectBiService);
    const autnService       = useSelector(selectAuthenticationService);
    
    
    // ------------------------------------------- methods -------------------------------------------

    const initState = useCallback(async () => {
        try{
            const response = await biService.bi();
            dispatch(INIT_INFO(response.data));
        } catch(err) {
            messageService.err(err);
        }
    }, [biService, messageService]);

    // ------------------------------------------- init -------------------------------------------
    
    useEffect(() => { 
        initState();
    }, [initState]);


    // ------------------------------------------- return statement -------------------------------------------
      
    return !autnService.isAdmin() ?
        (<Navigate to='/subredditals'/>) : 
        (<Bi 
            {...state.info}
        />);
    
}
