import  actions from './actionEnum';
import {INIT_INFO} from './bi.actions';

describe('bi actions', () => {

    it('INIT_INFO', () => {
        const info = {numOfUsers: 69, numOfPosts:42}

      expect(INIT_INFO(info)).toStrictEqual({type: actions.INIT_INFO , payload:info});
    });
  });