import React from 'react';
import { shallow } from 'enzyme';

import Bi from './bi.component';



//test to see that all the elements are bing rendered
describe('create-category component', () => {
  
  // create a new component
  const createComp = () =>
    shallow(
      <Bi
        newUsers={4} 
        newPosts= {2} 
        postsOfSub= {{Minecraft:4}}
      />
    );

    //test to see if components exist
    it('renders login', () => {
      const component = createComp();

      expect(component.find("div")      .length).toBe(4);
      expect(component.find("h3")       .length).toBe(2);
      expect(component.find("label")    .length).toBe(2);
    });
        
});