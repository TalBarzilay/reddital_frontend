import React, {useMemo} from 'react';
import styles from './bi.module.css';

import { Bar } from 'react-chartjs-2';

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';



ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

export default ({newUsers,newPosts, postsOfSub}) => { 
    
    if (!postsOfSub){
        return <div />
    }
    
    const labels = useMemo(() => Object.keys(postsOfSub)            , [postsOfSub]);
    const vals   = useMemo(() => labels.map(key => postsOfSub[key]) , [postsOfSub, labels]);

    const data = useMemo(() => ({
        labels, 
        datasets: [{
              label: 'posts',
              borderColor: 'rgba(0,0,0,1)',
              borderWidth: 2,
              backgroundColor: ["red", "turquoise", "pink", "blue", "purple", "grey", "brown", "yellow", "green",], 
              data: vals,
        }]
    }), [labels, vals])

    const options = useMemo(() => ({
         title:{
          display:false,
          text:'new posts per sub in last month',
          fontSize:20
        },
        legend:{
          display:false,
          position:'right',
        }
      }), []);
    
    
    return (
        <div className={`${styles.bi}`}>
            
            <div className={`${styles.biElement}`}>
                <h3 className={`${styles.noMargin} ${styles.text}`}>new users this month : </h3>
                <label className={`${styles.text}`}>{newUsers}</label>
            </div>

            <div className={`${styles.biElement}`}>
                <h3 className={`${styles.noMargin} ${styles.text}`}>new posts this month : </h3>
                <label className={`${styles.text}`}>{newPosts}</label>
            </div>

            <div className={`${styles.graphContainer}`}>
                <Bar
                    data={data}                 
                    options={options}
                    width={null}
                    height={60}
                />
            </div>
        </div>
    )
}
