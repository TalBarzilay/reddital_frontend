import React from 'react';
import { useMatch, useLocation, useNavigate } from "react-router";

export const withRouter = path  => Child => {
  return ( props ) => {
    const location  = useLocation();
    const navigate  = useNavigate();
    const match     = useMatch(path); 
    return <Child { ...props } match={match} navigate={ navigate } location={ location } />;
  }
}