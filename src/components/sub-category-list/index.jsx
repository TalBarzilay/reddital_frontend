import React, {useCallback} from 'react'
import SubCategoryList from './sub-category-list.component';

import { useSelector } from 'react-redux';
import { selectCategoryService, selectMessageService, selectAuthenticationService } from '../../redux/service/service.selectors';

import { useNavigate } from 'react-router-dom';

export default ({name, subredditals}) => {

    // ------------------------------------------- state -------------------------------------------

    const messageService    = useSelector(selectMessageService);
    const categoryService   = useSelector(selectCategoryService);
    const authService       = useSelector(selectAuthenticationService);

    const navigate = useNavigate();


    // ------------------------------------------ methods ------------------------------------------

    const deleteCategory = useCallback(async () => {
        try{
            await categoryService.deleteCategory(name);
            messageService.success('category deleted succesfully');

            navigate('/');
        } catch(err) {
            messageService.err(err);
        }
    }, [categoryService, messageService, navigate, name])
    
    
    // ------------------------------------------ return statement ------------------------------------------
    
    return (
        <SubCategoryList  
            name={name} 
            subredditals={subredditals}

            onCategoryDelete = {deleteCategory}
            isAdmin = {authService.isAdmin()}
        />
    )
}
