import React from 'react';
import { shallow } from 'enzyme';

import SubCategoryList from './sub-category-list.component'

describe('sub-category-list component', () => {

    const createComponent = () =>
    shallow(
      <SubCategoryList
        name = {"Gaming"} 
        subredditals = {[]}  
      />
    );

    // test that reducer returns currect inital state without arguments
    it('render sub-category-list', () => {
        const catList = createComponent();

        expect(catList.find("div").length).toBe(4);
        expect(catList.find("p").length).toBe(1);
    });

});