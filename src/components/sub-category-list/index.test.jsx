import React from 'react';
import { mount } from 'enzyme';
import {BrowserRouter as Router} from 'react-router-dom';

import {Provider} from 'react-redux';
import store from '../../redux/store'; 

import SubCategoryList from './index';



//test to see that all the elements are bing rendered
describe('sub-category-list index', () => {

    // create a new login component
    const createComponent = () =>
        mount( 
            <Provider store={store}>
                <Router>
                    <SubCategoryList
                        name = {"Gaming"}
                        subredditals = {[   {name:'minecraft'   , id:0}, 
                                            {name:'terreria'    , id:1}, 
                                            {name:'Half-Life3'  , id:2}]}
                    />
                </Router>
            </Provider>
        );



    // test creation format
    it('creation', () => {

        let wrper     = createComponent();
        let priviewer = wrper.find("SubCategoryList");

        expect(priviewer.length).toEqual(1);
    });
});