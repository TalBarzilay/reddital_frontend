import React from 'react'
import { Link } from 'react-router-dom';
import styles from './sub-category-list.module.css';

export default function SubCategoryList({name, subredditals, onCategoryDelete, isAdmin}) {
    return (
        <div className={`${styles.CategoryContainer}`} >
            <div className={`${styles.grandTitleContainer} ${isAdmin ? styles.spaceBetween: null}`}>
                <div className={`${styles.titleContainer}`}>
                    <p className={`${styles.categoryName}  playfair-font`}>{name}</p>
                </div>
                
                {isAdmin ?
                    <div className={`${styles.deleteButtonContainer}`}>
                        <button className={`${styles.deleteButton}`}  onClick={onCategoryDelete}>&#215;</button>
                    </div>
                    : null
                }
            </div>
            <div className={`${styles.subList}`}>
                {subredditals.map(sub => <Link key={sub.id} className={`${styles.SubLink} poppings-font`} to={`/${sub.name}/posts`} >{sub.name}</Link>)}
            </div>
        </div>
    )
}
