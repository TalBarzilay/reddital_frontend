import React from 'react'

import styles from './post-preview.module.css';

export default function PostPreview({title, content, creation, lastUpdated, blocked, username, onPostPriviewClick}) {
    
    return (
        <div className={`${styles.postPriviewContainer} ${blocked? styles.grayd : null}`} onClick={onPostPriviewClick}>
            
            <div className={`${styles.mainInfoContainer}`}>
                <h3 className={`${styles.title} shipori-font`}>{title}</h3>
                <h5 className={`${styles.creator} shipori-font`}>by {username} at {creation}</h5>
            </div>

            <div className={`${styles.lastUpdatedContainer}`}>
                <h5 className={`${styles.lastUpdatedTitle} shipori-font`}>last update :</h5>
                <h5 className={`${styles.lastUpdated} shipori-font`}>{lastUpdated}</h5>
            </div>
            
            <p className={`${styles.contentText} shipori-font`}>{content}</p>         
        </div>
        
    )
};
