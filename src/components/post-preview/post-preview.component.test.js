import React from 'react';
import { shallow } from 'enzyme';

import PostPreview from './post-preview.component';



//test to see that all the elements are bing rendered
describe('post-preview component', () => {

    // create a new component
    const createComponent = () =>
        shallow(
            <PostPreview 
                title={'I am a giant cat that LOVES pineapple. AMA'} 
                content={"my cat loves pineapples.\nask a question about my cat, and I'll answer it"} 
                creation={1637427478} 
                lastUpdated={1637477478} 
                username={"TalKing"}
            />
  );

    //test to see if components exist
    it('renders post-priview', () => {     
      const header = createComponent();

      expect(header.find("div").length).toBe(3);
      expect(header.find("h3").length).toBe(1);
      expect(header.find("h5").length).toBe(3);
      expect(header.find("p").length).toBe(1);
      
    });       
});