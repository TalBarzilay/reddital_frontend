import React from 'react';
import { mount } from 'enzyme';
import {BrowserRouter as Router} from 'react-router-dom';

import PostPreview from './index';



//test to see that all the elements are bing rendered
describe('PostPreview index', () => {

    // create a new login component
    const createComponent = () =>
        mount( 
            <Router>
                <PostPreview
                    title={'I am a giant cat that LOVES pineapple. AMA'}
                    content={"my cat loves pineapples.\nask a question about my cat, and I'll answer it"} 
                    creation={"20/11/2021, 18:57:58"} 
                    lastUpdated={"21/11/2021, 08:51:18"} 
                    username={"TalKing"}
                />
            </Router>
        );



    // test creation format
    it('creation date format', () => {

        let wrper     = createComponent();
        let priviewer = wrper.find("PostPreview");

        expect(priviewer.length).toEqual(1);
        expect(priviewer.prop("creation")).toBe("20/11/2021, 18:57:58");
    });

    // test last update format
    it('lastUpdated date format', () => {

        let wrper     = createComponent();
        let priviewer = wrper.find("PostPreview");

        expect(priviewer.length).toEqual(1);
        expect(priviewer.prop("lastUpdated")).toBe("21/11/2021, 08:51:18");
    });

});