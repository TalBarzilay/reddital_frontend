import React from 'react';
import PostPreview from './post-preview.component';

import { useNavigate } from 'react-router-dom';

export default (props) => {
    
    const navigate = useNavigate();
    
    return (
        <PostPreview
            {...props}

            onPostPriviewClick = {() => navigate(`/${props.id}/comments`)}
        />)
};