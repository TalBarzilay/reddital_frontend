import React from 'react';

import styles from './emote-box.module.css';

import Emote from '../emote';

export default ({emoteList}) => {
    
    return (
        <div className={`${styles.emoteBoxContainer}`}>
            {          
                emoteList.map(emoteBuilder =>
                    <Emote key={emoteBuilder.id} image={emoteBuilder.img}     counter={emoteBuilder.counter}       onClick={emoteBuilder.click} activated={emoteBuilder.isActivated} />
                )
            }
        </div>
    )
}
