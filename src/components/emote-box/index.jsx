import React from 'react';

import EmoteBox from './emote-box.component';

export default (props) => {
    return (
        <EmoteBox {...props} />
    )
}
