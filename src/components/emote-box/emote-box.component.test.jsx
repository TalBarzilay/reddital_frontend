import React from 'react';
import { shallow } from 'enzyme';

import EmoteBox from './emote-box.component';



//test to see that all the elements are bing rendered
describe('emote-box component', () => {

    // create a new component
    const createComponent = () =>
        shallow(
            <EmoteBox
              emoteList = {[]}
            />
  );

    //test to see if components exist
    it('renders emote-bx', () => {     
      const comp = createComponent();

      expect(comp.find("div").length).toBe(1);  
    });       
});