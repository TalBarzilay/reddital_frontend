import React from 'react';

import EmoteBox from '../../emote-box';

import like from '../../../resources/like.png';
import dislike from '../../../resources/dislike.png';

export default ({id, emotes, upVotes, downVotes, onCommentLike, onCommentDislike }) => {

    const emoteList = [
        {
            id: id + "_134745484273941708796857",
            img:like,
            counter:upVotes,
            click:() => onCommentLike(id),
            isActivated: emotes.includes("upvote")
        },
        {
            id: id + "_248745784790343278",
            img:dislike,
            counter:downVotes,
            click:() => onCommentDislike(id),
            isActivated: emotes.includes("downvote")
        }
    ];


    return (
        <EmoteBox emoteList={emoteList}/>
    )
}
