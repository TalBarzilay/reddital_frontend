import reducer from './comment-view.reducer';
import getPayload from './comment-view.actions';

describe('comment-view reducer', () => {

    it('reduce no input', () => {     
      expect(reducer()).toStrictEqual({showComments:true, showCommentReplyBox:false, showCommentEditBox:false, commentingText:""});
    });  
    
    it('reduce TOGGLE_COMMENTS', () => {     
      expect(reducer({showComments:true , showCommentReplyBox:false, showCommentEditBox:false, commentingText:""      }    ,getPayload().TOGGLE_COMMENTS())).toStrictEqual({showComments:false, showCommentReplyBox:false , showCommentEditBox:false, commentingText:""     });
      expect(reducer({showComments:false, showCommentReplyBox:false,showCommentEditBox:false, commentingText:""      }    ,getPayload().TOGGLE_COMMENTS())).toStrictEqual({showComments:true , showCommentReplyBox:false , showCommentEditBox:false, commentingText:""     });
      expect(reducer({showComments:true , showCommentReplyBox:true  ,showCommentEditBox:false, commentingText:"aaab" } ,getPayload().TOGGLE_COMMENTS())).toStrictEqual({showComments:false, showCommentReplyBox:true  , showCommentEditBox:false, commentingText:"aaab"    });
    }); 

    it('reduce TOGGLE_REPLY', () => {     
      expect(reducer({showComments:true , showCommentReplyBox:false , showCommentEditBox:false, commentingText:""     }    ,getPayload().TOGGLE_REPLY())).toStrictEqual({showComments:true , showCommentReplyBox:true  , showCommentEditBox:false, commentingText:""     });
      expect(reducer({showComments:false, showCommentReplyBox:true  , showCommentEditBox:false, commentingText:""     }    ,getPayload().TOGGLE_REPLY())).toStrictEqual({showComments:false, showCommentReplyBox:false , showCommentEditBox:false, commentingText:""     });
      expect(reducer({showComments:false, showCommentReplyBox:true  , showCommentEditBox:false, commentingText:"aaab" } ,getPayload().TOGGLE_REPLY())).toStrictEqual({showComments:false, showCommentReplyBox:false , showCommentEditBox:false, commentingText:"aaab" });
    });

    it('reduce TOGGLE_EDIT', () => {     
      expect(reducer({showComments:true , showCommentReplyBox:false , showCommentEditBox:false, commentingText:""     }    ,getPayload().TOGGLE_EDIT())).toStrictEqual({showComments:true , showCommentReplyBox:false  , showCommentEditBox:true, commentingText:""     });
      expect(reducer({showComments:false, showCommentReplyBox:true  , showCommentEditBox:true, commentingText:""     }    ,getPayload().TOGGLE_EDIT())).toStrictEqual({showComments:false, showCommentReplyBox:true , showCommentEditBox:false, commentingText:""     });
      expect(reducer({showComments:false, showCommentReplyBox:false  , showCommentEditBox:true, commentingText:"aaab" } ,getPayload().TOGGLE_EDIT())).toStrictEqual({showComments:false, showCommentReplyBox:false , showCommentEditBox:false, commentingText:"aaab" });
    });
    
    it('reduce COMMENT_CHANGE', () => {     
      expect(reducer({showComments:true , showCommentReplyBox:false , showCommentEditBox:false, commentingText:""     }    ,getPayload().COMMENT_CHANGE("abc")))      .toStrictEqual({showComments:true , showCommentReplyBox:false , showCommentEditBox:false, commentingText:"abc"    });
      expect(reducer({showComments:false, showCommentReplyBox:true  , showCommentEditBox:false, commentingText:"abc"  }    ,getPayload().COMMENT_CHANGE("")))         .toStrictEqual({showComments:false, showCommentReplyBox:true , showCommentEditBox:false, commentingText:""        });
      expect(reducer({showComments:false, showCommentReplyBox:true  , showCommentEditBox:false, commentingText:"aaab" } ,getPayload().COMMENT_CHANGE("asdfghj")))  .toStrictEqual({showComments:false, showCommentReplyBox:true , showCommentEditBox:false, commentingText:"asdfghj" });
    }); 
});