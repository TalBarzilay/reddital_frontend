import React from 'react';
import { shallow, mount } from 'enzyme';
import {BrowserRouter as Router} from 'react-router-dom';

import CommentView from './index';

import {Provider} from 'react-redux';
import store from '../../redux/store'; 




jest.mock('../../services/message.service', () => jest.fn().mockImplementation(() => ({
  info            : jest.fn(),
  success         : jest.fn(),
  warn            : jest.fn(),
  error           : jest.fn(),
  err             : jest.fn(),
  addNotification : jest.fn(),
})));

jest.mock('../../services/post.service', () => jest.fn().mockImplementation(() => ({
  replyToComment  : jest.fn(),
  upvoteComment   : jest.fn(),
  downvoteComment : jest.fn(),
})));

jest.mock('../../redux/posts-of-sub/posts-of-sub.selectors', () => ({
  selectPost: (a,b) => ({
      id: 77,
      title: 'for the one thiks Spongebob Squarepants will be a good president of the USA',
      content:"I'll have to disagree. Spongebob Squarepants will be a TERIBLE presidet.", 
      creation:"20/01/2021, 00:50:28", 
      lastUpdated:"23/02/2021, 00:50:28",
      username:"TalKing",
      upVotes:42,
      downVotes:69,
      emotes:[],
      comments:[]
    })
}));

jest.mock('../../redux/posts-of-sub/posts-of-sub.utils', () => ({
  getComment: (a,b) => ({
      id: 77,
      content:"comment!", 
      creation:"20/01/2021, 00:50:28", 
      updated:"23/02/2021, 00:50:28",
      username:"TalKing",
      upVotes:42,
      downVotes:69,
      emotes:[],
      comments:[]
    })
}));


jest.mock('react-router', () => ({
  ...jest.requireActual('react-router'),
  useMatch: () => ({
    params: {postid:4}
  })
}));

describe('comment-view index', () => {

  // create a new component
 const createIndex = () =>
   mount(<Provider store={store}><Router><CommentView 
      comments={[]}
      id={42}
      content={"I'll have to disagree. Spongebob Squarepants will be a TERIBLE presidet."}
      creation={1637427478 }
      lastUpdated={1637477478}
      user={"TalKing"}
      upVotes={42}
      downVotes={69}
      emotes={[]}
    /></Router></Provider>);

  const getInner = wrapper => wrapper.find("_default").at(0);

 
 it('toggle reply box', () => {     
     let wrper         = createIndex();
     let view          = getInner(wrper);
     let toggleButton  = wrper.find('.replybutton').at(0);

     expect(toggleButton.length).toEqual(1);
     expect(view.length).toEqual(1);
     expect(view.prop("showCommentReplyBox")).toBe(false);

     toggleButton.simulate("click");
     wrper.update();
     view          = getInner(wrper);
     expect(view.prop("showCommentReplyBox")).toBe(true);

     toggleButton.simulate("click");
     wrper.update();
     view          = getInner(wrper);
     expect(view.prop("showCommentReplyBox")).toBe(false);
 });   
 
 
  it('toggle comments', () => {
    let wrper         = createIndex();
    let view          = getInner(wrper);
    let toggleButton  = wrper.find('.contentContainer').at(0);
   
    expect(toggleButton.length).toEqual(1);
    expect(view.length).toEqual(1);
    expect(view.prop("showComments")).toBe(true);

    toggleButton.simulate("click");
    wrper.update();
    view          = getInner(wrper);
    expect(view.prop("showComments")).toBe(false);

    toggleButton.simulate("click");
    wrper.update();
    view          = getInner(wrper);
    expect(view.prop("showComments")).toBe(true);
  });


  it('comment text changed', () => {
    let wrper         = createIndex();
    let view          = getInner(wrper);
    let toggleButton  = wrper.find('.replybutton').at(0);

   

    expect(toggleButton.length).toEqual(1);
    expect(view.length).toEqual(1);

    toggleButton.simulate("click"); //have comment text box be showen

    let textbox  = wrper.find('.replyBox').at(0);


    let value = "hello!";

    textbox.simulate("change", { target: { value } });
    wrper.update();
    view          = getInner(wrper);
    expect(view.prop("commentingText")).toBe(value);

    value = "hello!!";
    textbox.simulate("change", { target: { value } });
    wrper.update();
    view          = getInner(wrper);
    expect(view.prop("commentingText")).toBe(value);

    value = "hello!! I am leeroy Jenkins!";
    textbox.simulate("change", { target: { value } });
    wrper.update();
    view          = getInner(wrper);
    expect(view.prop("commentingText")).toBe(value);
  });
});