import React from 'react';
import { shallow } from 'enzyme';

import ComponentView from './comment-view.component';



//test to see that all the elements are bing rendered
describe('comment-view component', () => {

    // create a new component
    const createComponent = () =>
        shallow(
            <ComponentView
                recursiveRender={true}
                comment = {{
                  id:111,
                  content:"I'll have to disagree. Spongebob Squarepants will be a TERIBLE presidet.", 
                  creation:1637427478, 
                  lastUpdated:1637477478,
                  user:"TalKing",
                  upVotes:42,
                  downVotes:69,
                  emotes:[],
                  comments:[],
                }}
            />
  );

    //test to see if components exist
    it('renders comment-view', () => {     
      const comp = createComponent();

      expect(comp.find("div").length).toBe(4);
      expect(comp.find("h6").length).toBe(2);
      expect(comp.find("h5").length).toBe(1);
      expect(comp.find("button").length).toBe(1);     
    });       
});