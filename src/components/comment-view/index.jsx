import React , {useReducer, useCallback, useEffect} from 'react';
import reducer from './comment-view.reducer';
import getPayload from './comment-view.actions';

import { withRouter } from '../withRouter';

import { connect } from 'react-redux';
import { POST_COMMENT, UPDATE} from '../../redux/posts-of-sub/posts-of-sub.actions';
import {ADD_COMMENT} from '../../redux/todays-activities/todays-activities.actions';
import { getComment } from '../../redux/posts-of-sub/posts-of-sub.utils';
import { selectPost } from '../../redux/posts-of-sub/posts-of-sub.selectors';
import {selectAuthenticationService, selectMessageService, selectPostService} from '../../redux/service/service.selectors';

import CommentView from './comment-view.component';

const CommentViewIndex = ({comment, postid, authService, messageService, postService, className, recursiveRender, UPDATE, POST_COMMENT, ADD_COMMENT}) => {

    // --------------------------------- state ---------------------------------

    const [state, dispatch] = useReducer(reducer, reducer());

    // --------------------------------- init state ---------------------------------

    useEffect(() => 
        dispatch(getPayload().COMMENT_CHANGE(comment.content))
    ,[comment.content]);

    // --------------------------------- functions ---------------------------------


    /**
     * handke posting a comment
     * @param {*} state the current state
     */
    const handleCommenting = useCallback(async state => {
        let contnet = state.commentingText;

        if(contnet === ""){
            messageService.error('comment is missing', 'please enter a comment');
        } else {
            try{
                const response = await postService.replyToComment(contnet, comment.id);

                let newComment = response.data;
                newComment.comments = []; // replace null with empty list

                dispatch(getPayload().TOGGLE_REPLY());
                POST_COMMENT(comment.id, newComment); 
                ADD_COMMENT(newComment, postid); //save for today's activites  

                messageService.success('commented successfully!');
            } catch(err) {
                messageService.err(err);
            }
        }             
    }, [ADD_COMMENT, POST_COMMENT, comment.id, postid, messageService, postService]);


    const handleEdit = useCallback(async state => {
        let contnet = state.commentingText;

        if(contnet === ""){
            messageService.error('comment is missing', 'please enter a comment');
        } else {
            try{
                const response = await postService.editComment(comment.id, contnet);


                UPDATE(response.data);
                dispatch(getPayload().TOGGLE_EDIT());

                messageService.success('edited successfully!');
            } catch(err) {
                messageService.err(err);
            }
        }             
    }, [UPDATE, comment.id, messageService, postService]);


    const handleUpvote = useCallback(async id => {
        try{
            const response = await postService.upvoteComment(id);
            UPDATE(response.data);   
        } catch(err){
            messageService.err(err);
        }
    }, [UPDATE, messageService, postService]);

    const handleDownvote = useCallback(async id => {
        try{
            const response = await postService.downvoteComment(id);
            UPDATE(response.data);   
        } catch(err){
            messageService.err(err);
        }
    }, [UPDATE, messageService, postService]);
    
     // --------------------------------- return statement ---------------------------------

     const user = authService.getLoggedUser();

    return (
        <CommentView
            {...state}

            comment = {comment}

            className = {className}
            recursiveRender = {recursiveRender === false? false : true} 

            toggleComments      = {() => dispatch(getPayload().TOGGLE_COMMENTS())}
            togleReply          = {() => dispatch(getPayload().TOGGLE_REPLY())}
            OnToggleEditClick   = {() => dispatch(getPayload().TOGGLE_EDIT())}

            onCommentChange =  {event => dispatch(getPayload().COMMENT_CHANGE(event.target.value))}
            
            onPostCommentClick = {() => handleCommenting(state)}
            onCommentEditClick = {() => handleEdit(state)}

            onCommentLike      = {handleUpvote} 
            onCommentDislike   = {handleDownvote}

            isAdmin={authService.isAdmin()}
            isOwnComment = { user && comment ? user.username === comment.user : false}
            
        />
    ) 
};

const mapStateToProps = (state, ownProps) => {
    const match = ownProps.match;
    const commentid = parseInt(ownProps.id);
    const className = ownProps.className;

    let comment = ownProps.comment;
    let postid  = ownProps.postid;

    if(!comment){
        postid          = parseInt(match.params.postid);  
        const post      = selectPost(state, postid);
        comment         = getComment(post, commentid);
    }

    const messageService    = selectMessageService(state);
    const postService       = selectPostService(state);
    const authService       = selectAuthenticationService(state);

    return {comment, postid, className, messageService, postService, authService};
};

const mapDistpatchToProps = dispatch => ({
    POST_COMMENT    : (commentid, comment)  => dispatch(POST_COMMENT(commentid, comment)),
    UPDATE          : commentable           => dispatch(UPDATE(commentable)),
    ADD_COMMENT     : (comment, postid)     => dispatch(ADD_COMMENT(comment, postid)),
});

export default withRouter('/:postid/comments')(connect(mapStateToProps, mapDistpatchToProps)(CommentViewIndex));

