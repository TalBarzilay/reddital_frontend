const actions = {
    TOGGLE_COMMENTS         : 'COMMENT_VIEW_TOGGLE',
    TOGGLE_REPLY            : 'COMENT_VIEW_TOGGLE_REPLY',
    TOGGLE_EDIT             : 'TOGGLE_EDIT',
    COMMENT_CHANGE          : 'COMENT_VIEW_COMMENT_CHANGE',
};

export default actions;