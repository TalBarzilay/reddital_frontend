import React from 'react';

import CommentView from './index';

import styles from './comment-view.module.css';

import reply from '../../resources/reply.png';
import edit from '../../resources/edit.png';

import CommentEmoteBox from './comment-emote-box';


export default ({comment, className, commentingText, recursiveRender, showComments, isAdmin, isOwnComment, showCommentReplyBox, showCommentEditBox, OnToggleEditClick, onCommentEditClick, toggleComments, togleReply, onCommentChange, onPostCommentClick, onCommentLike, onCommentDislike}) => {

    if(!comment){
        return <div />; // for whatever resun this component is being rendered before initialization....
    } 
    
    const {id, content, user, updated, upVotes, downVotes, emotes, comments} = comment;

    return (
        <div className={`${styles.CommentContainer} ${className}`}>
            <div className={`${styles.userTimeContainer}`}>
                <h6 className={`${styles.user}`}>{user}</h6>
                <h6 className={`${styles.updated}`}>{updated}</h6>
            </div>

            <div className={`${styles.contentContainer}`} onClick={toggleComments}>
                <h5 className={`${styles.content}`}>{content}</h5>
            </div>
            
            {!recursiveRender ? null : 
                <div className={`${styles.replybuttonContainer}`} >
                    <button id={`button_${comment.id}`} onClick={togleReply} className={`${styles.replybutton}`}><img className={`${styles.replyImg}`} src={reply} alt="" /></button>
                    {isAdmin || isOwnComment?
                        <button onClick={OnToggleEditClick} className={`${styles.editbutton}`}><img className={`${styles.editImg}`} src={edit} alt="" /></button>
                        : null
                    }    
                    <CommentEmoteBox id={id} emotes={emotes} upVotes= {upVotes} downVotes={downVotes} onCommentLike={onCommentLike} onCommentDislike={onCommentDislike}/>
                </div>
            }

            {
                showCommentReplyBox ?
                    <div className={`${styles.replyContainer}`}>
                        <input className={`input ${styles.replyBox}`} placeholder="comment now..." onChange= {onCommentChange}></input>
                        <button className={`buton ${styles.replyButton}`} onClick={onPostCommentClick}>Comment</button>
                    </div>
                : undefined
            }
            {
                showCommentEditBox ?
                    <div className={`${styles.editContainer}`}>
                        <input className={`input ${styles.editBox}`} placeholder="edit" value={commentingText} onChange= {onCommentChange}></input>
                        <button className={`buton ${styles.edditingButton}`} onClick={onCommentEditClick}>Edit</button>
                    </div>
                : undefined
            }
            {!recursiveRender ? 
                null :
                comments.map(cur_comment => <CommentView  className={`${showComments ? undefined : styles.hide}`} key={cur_comment.id} id={cur_comment.id}/>)
            }
        </div>
    )
}
