import actions from './actionEnum';
import getPayload from './comment-view.actions'

describe('comment view actions', () => {

    it('test get payload', () => {    
      let comment = {id:42, content:"hello?"}
      

      expect(getPayload().TOGGLE_COMMENTS())              .toStrictEqual({type: actions.TOGGLE_COMMENTS       , payload: {}});
      expect(getPayload().TOGGLE_REPLY())                 .toStrictEqual({type: actions.TOGGLE_REPLY          , payload: {}});
      expect(getPayload().COMMENT_CHANGE(comment))        .toStrictEqual({type: actions.COMMENT_CHANGE        , payload: comment});
    });       
});