import React from 'react';
import { shallow } from 'enzyme';

import Emote from './emote.component';

import like from '../../resources/like.png';



//test to see that all the elements are bing rendered
describe('emote component', () => {

    // create a new component
    const createComponent = () =>
        shallow(
            <Emote
                image = {like}
                counter = {23}
                onClick = {() => {}}
            />
  );

    //test to see if components exist
    it('renders emote', () => {     
      const comp = createComponent();

      expect(comp.find("div").length).toBe(3);
      expect(comp.find("button").length).toBe(1);
      expect(comp.find("h5").length).toBe(1);    
    });       
});