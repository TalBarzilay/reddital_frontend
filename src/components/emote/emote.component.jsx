import React from 'react';
import styles from './emote.module.css';

export default ({image, counter, onClick, activated}) => {
    return (
        <div className={`${styles.emoteContainer}`}>
            <div className={`${styles.buttonContainer}`}>
                <button onClick={onClick} className={`${styles.emoteButton}`}><img className={`${styles.emoteImg}`} src={image} alt="" /></button>
            </div>
            <div className={`${styles.counterContainer} ${activated ? styles.activated : ""}`}>
                <h5 className={`${styles.emoteCounter}`}>{counter}</h5>
            </div>
        </div>
    )
}
