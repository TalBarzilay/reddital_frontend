import React from 'react';
import Emote from './emote.component';

export default(props) => {
    return (
       <Emote {...props} />
    )
}
