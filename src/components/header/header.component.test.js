import React from 'react';
import { shallow } from 'enzyme';

import Header from './header.component';



//test to see that all the elements are bing rendered
describe('header component', () => {

    // create a new component
  const createHeader = () =>
  shallow(
    <Header
    />
  );

    //test to see if components exist
    it('renders header', () => {     
      const header = createHeader();

      expect(header.find("Link").length).toBe(3);
      expect(header.find("button").length).toBe(2);
      expect(header.find("h2").length).toBe(1);
    });       
});