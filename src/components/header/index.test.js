import React from 'react';
import { mount } from 'enzyme';
import Header from './index';
import {BrowserRouter as Router} from 'react-router-dom';

import {Provider} from 'react-redux';
import store from '../../redux/store'; 



//test to see that all the elements are bing rendered
describe('header index', () => {

   // create a new index component
  const createIndexComponent = () =>
    mount(<Provider store={store}><Router><Header useForceUpdate = {() => {() => {}}}/></Router></Provider>);


    //test to see if components exist
    it('renders index', () => {     
        const wrapper = createIndexComponent();

        expect(wrapper.length).toEqual(1);
    });       
});