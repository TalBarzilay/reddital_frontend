import React from 'react'
import { Link } from 'react-router-dom';
import styles from './header.module.css';

export default ({clientName, signInOutText, signInOutAction, profileAction, isAdmin}) => {
    return (
        <div className={styles.headerContainer}>
            <div className={styles.innerheaderContainer}>

                <div className={styles.welcomeContainer}>
                    <h2 className={`${styles.welcome} playfair-font`}>Welcome, </h2>
                    <button className={`link ${styles.headerlink} ${styles.profilebutton} playfair-font`} onClick={profileAction}>{clientName}</button> 
                </div>

                
                {isAdmin ? 
                    <Link className= {`${styles.headerlink} playfair-font`} to="/adminHub">Admin Hub</Link>
                    : null
                }

                <Link className= {`${styles.headerlink} playfair-font`} to="/subredditals">Subredditals</Link>
                <Link className= {`${styles.headerlink} playfair-font`} to="/todaysActivites">Activites</Link>
                <Link className= {`${styles.headerlink} playfair-font`} to="/aboutme">About me</Link>
            

                <button className={`link ${styles.headerlink} playfair-font`} onClick={signInOutAction}>{signInOutText}</button> 
            </div>     
        </div>
    )
}
