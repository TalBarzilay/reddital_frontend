import React, { useCallback} from 'react';
import { useNavigate } from 'react-router';

import {useSelector, useDispatch } from 'react-redux';
import { selectMessageService, selectAuthenticationService } from '../../redux/service/service.selectors';
import { RESET } from '../../redux/todays-activities/todays-activities.actions';

import Header from './header.component';

const HeaderIndex =  () => {

    // ------------------------------------------- state -------------------------------------------

    const navigate      =  useNavigate();

    const dispatch      = useDispatch();

    const messageService        = useSelector(state => selectMessageService(state));
    const authenticationService = useSelector(state =>selectAuthenticationService(state));

    const user = authenticationService.getLoggedUser();

    // ------------------------------------------- functions -------------------------------------------

    const logout = useCallback(() => {
        authenticationService.logout();
        dispatch(RESET()); //remove today's activities 
        
        messageService.success('Logged out', 'Goodbye :)'); 
        navigate('/login');
    }, [authenticationService,messageService, navigate, dispatch]);

    
    // ------------------------------------------- return statement -------------------------------------------

    return (
        <Header
            clientName = {(user === undefined || user === null) ? "Guest" : user.username}
            signInOutText = {(user === undefined || user === null) ? "Login" : "Logout"}
            signInOutAction = {(user === undefined || user === null) ? 
                () => { navigate('/login'); } : 
                logout
            }
            profileAction = {  () => (user === undefined || user === null) ?  navigate('/signup') : navigate('/profile') }
            isAdmin={ authenticationService.isAdmin()}
        />
    )
};

export default HeaderIndex;
