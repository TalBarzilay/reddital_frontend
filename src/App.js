import React  from 'react';

import './App.css';

import Register           from './pages/register';
import Login              from './pages/login';
import Posting            from './pages/posting';
import PostList           from './pages/post-list';
import CategoryList       from './pages/category-list';
import AboutMe            from './pages/about-me';
import PostComponentView  from './pages/post-comments-view';
import TodaysActivites    from './pages/todays-activites';
import CreateCategory     from './pages/create-category';
import CreateSubreddital  from './pages/create-subreddital';
import BlockUser          from './pages/block-user';
import Profle             from './pages/profile';
import BI                 from './pages/bi';
import AdminHub           from './pages/admin-hub';

import Header from './components/header';

import {Route, Routes, Navigate} from 'react-router-dom';

import ReactNotification from 'react-notifications-component';

import 'react-notifications-component/dist/theme.css'
import 'animate.css';


function App() {

  return (
    <div className="App">
      <ReactNotification />
      <Header />
        <Routes>
          
          <Route exact path="/"                   element={<Navigate to ="/signup" />}/>
          <Route exact path='/signup'             element={<Register />} />
          <Route exact path='/login'              element={<Login />} />
          <Route exact path="/:subreddit/post"    element={<Posting />} />
          <Route exact path="/:subreddit/posts"   element={<PostList/> } />
          <Route exact path="/aboutme"            element={<AboutMe  /> } />
          <Route exact path="/subredditals"       element={<CategoryList  /> } />
          <Route exact path="/:postId/comments"   element={<PostComponentView /> } />
          <Route exact path="/todaysActivites"    element={<TodaysActivites /> } />
          <Route exact path="/createCategory"     element={<CreateCategory /> } />
          <Route exact path="/createSubreddital"  element={<CreateSubreddital /> } />
          <Route exact path="/blockUser"          element={<BlockUser /> } />
          <Route exact path="/profile"            element={<Profle /> } />
          <Route exact path="/bi"                 element={<BI /> } />
          <Route exact path="/adminHub"           element={<AdminHub /> } />
      
        </Routes>
    </div>
  );
}

export default App;
 