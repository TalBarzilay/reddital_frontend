import webRequestService from "./web-request.service";

class authenticationService {
     
    /** the URI for the user actions gateway */
    #userURI = "";

    /** the key in the storage device to get the authentication key with */
    #logedUser      = "logedUser";
    #accessToken    = "access_token";
    #refreshToken   = "refresh_token";
    
    /** the storage to use to save session data */
    #storage = localStorage;



    static classInstance = null; // the instance, as of the singletone design pattern

    /** get the only instance if this class, as of the singletone design pattern */
    static getInstance() {
        if (authenticationService.classInstance === null) {
            authenticationService.classInstance = new authenticationService();
        }
        return this.classInstance;
    }





    
    /**
     * @returns the current user's auth key
     */
    getLoggedUserAuthKey() {
        return this.#storage.getItem(this.#accessToken);
    }

     /**
     * @returns the current user's refresh key
     */
    getRefreshToken() {
        return this.#storage.getItem(this.#refreshToken);
    }

    /**
     * @returns the current user'
     */
    getLoggedUser() {
        return JSON.parse(this.#storage.getItem(this.#logedUser));
    }

    /**
     * 
     * @returns a list of the current user's authorites
     */
    getUserAuthorities(){
        const user = this.getLoggedUser();
        return user? user.roles : [];
    }

    /**
     * @return if admin is loged in
     */
    isAdmin(){
        return this.getUserAuthorities().includes('ADMIN');
    }

    /**
     * delete cookies
     */
    logout() {
        this.#storage.removeItem(this.#accessToken);
        this.#storage.removeItem(this.#refreshToken);
        this.#storage.removeItem(this.#logedUser);
    }

    /**
     * @param {*} user the user
     */
    setSession(user, accessToken, refreshToken) {
        this.setUser(user);
        this.#storage.setItem(this.#accessToken, accessToken);
        this.#storage.setItem(this.#refreshToken, refreshToken);
    }

     /**
     * setting acess toekn
     */
    setAccessToken(accessToken) {
        this.#storage.setItem(this.#accessToken, accessToken);
    }

    setUser(user){
        this.#storage.setItem(this.#logedUser, JSON.stringify(user));
    }
    







    
    /**
     * perform a signup method
     * @param {*} username the username
     * @param {*} email the email
     * @param {*} password the password
     * @returns an HTML promise of the requesr
     */
    signup(username, email, password) {
        return webRequestService.getInstance().post(this.#userURI + "/signup", {username, email, password})
    }

    /**
     * permenetlly ban a user
     * @param {*} username the user name to ban
     */
    block(username) {
        return webRequestService.getInstance().post(this.#userURI + "/blockUser", {}, {username})
    }

    /**
     * perform a login method
     * @param {*} username the username
     * @param {*} password the password
     * @returns an HTML promise of the requesr
     */
    async login(username, password) {
        try {
            const response = await webRequestService.getInstance().post(this.#userURI + "/login", {username, password});

            const user      = response.data;

            const access    = response.headers[this.#accessToken];
            const refresh   = response.headers[this.#refreshToken];
            
            this.setSession(user, access, refresh);

            return response;
        } catch(err) {
            throw err;
        }

    }

};




export default authenticationService;