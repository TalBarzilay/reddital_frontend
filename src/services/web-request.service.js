import axios from 'axios';

import authenticationService from './authentication.service';
import messageService from './message.service';

/** path to refreshing acess token */
const refreshURL = '/refreshAccessToken';

/** the URL of the server to send the requests to */
const url = process.env.REACT_APP_BACKEND_BASE_URL || "http://localhost:8080";

const authService = authenticationService.getInstance();


const request = axios.create({
    baseURL: url,
  });


/** if requesr failed, try refreshing acess token, id still failed ->  error */
request.interceptors.request.use(

    /** attach acess token to each response */
    config => {
        let token = authService.getLoggedUserAuthKey();

        // if the refresh token pathm do not inject acess token, as it is expired.
        if (token !== null && config.url !== refreshURL)  
            config.headers.Authorization = token;
        
        return config;
    },

    async error => {
        return Promise.reject(error);
    },
)


request.interceptors.response.use(

    config => {
        return config;
    },


    /** in case of error, try refreshing the access token */
    async error => {
        const originalRequest = error.config;
        
        if (error && error.response && (error.response.status === 401 || error.response.status === 403) && originalRequest && !originalRequest._retry) {
        
            originalRequest._retry = true; // to avoid infinte loop.
            const refresh_token = authService.getRefreshToken();

            if(!refresh_token) { // no refresh token, can't do much here.
                return Promise.reject(error);
            }

            try{
                const response = await request.post(refreshURL, {}, {headers: {Authorization: refresh_token}}); // refresh acess token      
                authService.setAccessToken(response.data); // set new acess token
                (new messageService()).warning('access token expired', 'acess token was refreshed');

                return request(originalRequest); // do the original reques once again, so if the error was because of expired acess token, it will be transperent to the user
            
            } catch(err) {
                authService.logout();
                originalRequest.headers = {}; // remove bad token from the original request

                return request(originalRequest); // mybe you don't need to be loged in ?
            }
        } else {
            return Promise.reject(error); // some other error occured, will be handle in upper level.
        }
    },


)











class webRequestService {
    

    static classInstance = null; // the instance, as of the singletone design pattern

    /** get the only instance if this class, as of the singletone design pattern */
    static getInstance() {
        if (webRequestService.classInstance === null) {
            webRequestService.classInstance = new webRequestService();
        }
        return this.classInstance;
    }
    









    /**
     * sent an HTML POST request
     * @param {*} uri the uri to send to
     * @param {*} payload the body of the requset (JSON)
     * @param {*} params optional url parameters (JSON)
     * @returns an HTML promise of the request
     */
    post(uri, payload, params = undefined) {
       return request.post(uri, payload, params === undefined? {} : {params});
    }

    /**
     * sent an HTML GET request
     * @param {*} uri th e uri to send to
     * @param {*} params optional url parameters (JSON)
     * @returns an HTML promise of the request
     */
     get(uri, params = {}) {
        return request.get(uri, {params});
     }
};




export default webRequestService;