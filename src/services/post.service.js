import webRequestService from "./web-request.service";

class postService {
     
    /** the URI for the posting actions gateway */
    #postURI    = "";
    #commetURI  = '/comments';


    /**
     * send a new post
     * @param {*} title the title of the post
     * @param {*} content the content of the post 
     * @param {*} subreddital the subreddital to post the post to
     */
    post(title, content, subreddital) {
        return webRequestService.getInstance().post(this.#postURI + "/posting", {title, content}, {subreddital})
    }

    /**
     * get all posts of a given subreddit
     * @param {*} subreddital the subreddits name
     * @returns a promise of the request
     */
    posts(subreddital) {
        return webRequestService.getInstance().get(this.#postURI + "/posts", {subreddital})
    }

    /**
     * get a post posts of a given subreddit
     * @param {*} id the id of the post
     * @returns a promise of the request
     */
     getPost(id) {
        return webRequestService.getInstance().get(this.#postURI + "/post", {id})
    }

    blockPost(id){
        return webRequestService.getInstance().post(this.#postURI + "/blockPost", {}, {id});
    }


    /**
     * post a reply to a comment
     * @param {*} content the contnet of the reply
     * @param {*} responseTo the id if the comment to response to
     */
    replyToComment(content, responseTo){
        return webRequestService.getInstance().post(this.#commetURI + '/commenting', {content, responseTo});
    }

    /**
     * post a reply to a post
     * @param {*} content the contnet of the reply
     * @param {*} postedOn the id if the comment to response to
     */
     replyToPost(content, postedOn){
        return webRequestService.getInstance().post(this.#commetURI + '/commenting', {content, postedOn});
    }


    /**
     * upvote a post
     * @param {*} id the id of the post to upvote
     * @returns a promise of the request
     */
    upvotePost(id){
        return this.emotePost(id, "upvote");
    }

    /**
     * downvote a comment
     * @param {*} id the id of the comment to downvote
     * @returns a promise of the request
     */
    downvoteComment(id){
        return this.emoteComment(id, "downvote");
    }

    /**
     * upvote a comment
     * @param {*} id the id of the comment to upvote
     * @returns a promise of the request
     */
     upvoteComment(id){
        return this.emoteComment(id, "upvote");
    }

    /**
     * downvote a post
     * @param {*} id the id of the post to downvote
     * @returns a promise of the request
     */
    downvotePost(id){
        return this.emotePost(id, "downvote");
    }

    /**
     * eddit a post
     * @param {*} id id of post
     * @param {*} title new title
     * @param {*} contnet new content
     */
    editPost(id, title, content){
        return webRequestService.getInstance().post(this.#postURI + "/editPost", {id, title, content})
    }

        /**
     * eddit a post
     * @param {*} id id of post
     * @param {*} title new title
     * @param {*} contnet new content
     */
    editComment(id, content){
            return webRequestService.getInstance().post(this.#commetURI + "/editComment", {id, content})
    }
    



    // ---------------------------------------------- private methods ----------------------------------------------

    /**
     * emote on a post
     * @param {*} id the id of the post to emote to
     * @param {*} emote the emote
     * @returns a promise of the request
     */
    emotePost(id, emote) {
        return webRequestService.getInstance().post(this.#postURI + '/emote', {id, emote});
    }

    /**
     * emote on a comment
     * @param {*} id the id of the post to emote to
     * @param {*} emote the emote
     * @returns a promise of the request
     */
     emoteComment(id, emote) {
        return webRequestService.getInstance().post(this.#commetURI + '/emote', {id, emote});
    }
}



export default postService;