import webRequestService from "./web-request.service";

class userService {
     
    /** the URI for the posting actions gateway */
    #userURI      = "";


    
    /**
     * edit profile info
     * @param {*} email new email
     * @returns 
     */
    editProfile(email) {
       return webRequestService.getInstance().post(this.#userURI + "/updateProfile", {}, {email})
    }

    /**
     * change user's password
     * @param {*} password current password
     * @param {*} oldPassword old password
     * @returns 
     */
    changePassword(password, oldPassword) {
        return webRequestService.getInstance().post(this.#userURI + "/changePassword", {password, oldPassword})
    }

}



export default userService;