import webRequestService from "./web-request.service";

class categoryService {
     
    /** the URI for the posting actions gateway */
    #categoriesURI      = "/categories";
    #subredditalsURL    = "/subredditals"


    /** 
     * @returns all subreditals 
     */
     subredditals() {
        return webRequestService.getInstance().get(this.#categoriesURI + "/all")
    }

    /** 
     * @returns get editabl categories by the user 
     */
    edditablCategories() {
        return webRequestService.getInstance().get(this.#categoriesURI + "/editable").then(categories => 
            categories.data.map(cat => cat.name)
        );
    }

     /** 
     * @returns create a caregory 
     */
    createCategory(category) {
        return webRequestService.getInstance().post(this.#categoriesURI + "/create", {}, {category});
    }

    /** 
     * @returns create a caregory 
     */
    deleteCategory(category) {
        return webRequestService.getInstance().post(this.#categoriesURI + "/delete", {}, {category});
    }

    /**
     * create sub reddit
     * @param {*} name the name of the sub
     * @param {*} category the category the sub belongs to
     * @returns 
     */
    createSub(name, category){
        return webRequestService.getInstance().post(this.#subredditalsURL + "/create", {name, category});
    }

    /**
     * delete a subreddital
     * @param {*} subreddital the subreddital to delete
     * @returns the deleted subreddital promise
     */
    deleteSub(subreddital){
        return webRequestService.getInstance().post(this.#subredditalsURL + "/delete", {}, {subreddital});
    }
}



export default categoryService;