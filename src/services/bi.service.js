import webRequestService from "./web-request.service";

class biService {
     
    /** the URI for the posting actions gateway */
    #biURI      = "";


    /** 
     * @returns bi info 
     */
     bi() {
        return webRequestService.getInstance().get(this.#biURI + "/bi")
    }
}



export default biService;