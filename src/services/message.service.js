import { store } from 'react-notifications-component';

class messageService {


    addNotification = (title, message=" ", type) => {
        store.addNotification({
            title: title,
            message: message,
            type: type,
            insert: "bottom",
            container: "bottom-right",
            animationIn: ["animate__animated", "animate__fadeIn"],
            animationOut: ["animate__animated", "animate__fadeOut"],
            width:500,
            dismiss: {
              duration: 5000,
              onScreen: true,
              showIcon:true,
              pauseOnHover: true
            }
        });     
    }






    success = (title, message) => {
        this.addNotification(title, message, 'success');
    }

    error = (title, message) => {
        this.addNotification(title, message, 'danger');
    }


    info = (title, message) => {
        this.addNotification(title, message, 'info');
    }

    warning = (title, message) => {
        this.addNotification(title, message, 'warning');
    } 
    
    
    
    

    err = message => {

        if (message && message instanceof String){
            this.error('ERROR', message);
        } else if(message && message.response && message.response.status && (message.response.status === 401 || message.response.status === 403)){
            this.error('ERROR', 'user could not be verified!');
        } else if(message && message.response && message.response.data){
            this.error('ERROR', message.response.data);
        } else {
            this.error('ERROR', 'error acured while proccesing the request.');
            if(message && message.response){
                console.log(message.response);
            } else {
                console.log(message);
            }
        
        }      
    } 
    
}



export default messageService;