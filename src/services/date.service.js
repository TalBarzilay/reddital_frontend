class dateService {

    /**
     * transform a time stemp to a date
     * @param {*} timestamp the time stamp
     * @returns a date object of the given time stamp
     */
    timestamp2date = (timestamp) => {
        return new Date(timestamp);
    }

    /**
     * transform a date object into a nice string
     * @param {*} date the date object
     * @returns the date in a string
     */
    date2string = (date) => {
        //const options = { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric', hour12: false };

        if (process.env.NODE_ENV === 'test')
            return date.toLocaleString("en-GB", {hour12: false, timeZone: "Israel"});
        else
            return date.toLocaleString("en-GB", {hour12: false});
    }
}




export default dateService;