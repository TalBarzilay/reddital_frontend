import reducer from './todays-activities.reducer';
import {ADD_POST, ADD_COMMENT, RESET} from './todays-activities.actions';

// test reducer function
describe('todays-activites reducer', () => {

  const posts = [
    {
        id : 42,
        title:'I am a giant cat that LOVES pineapple. AMA',
        content:"my cat loves pineapples.\nask a question about my cat, and I'll answer it",
        creation:1637428478,
        lastUpdated:1637477478,
        username:"TalKing",
        comments: [
            {
                id : 42,
                content:"ok...?",
                creation:1637428478,
                updated:1637477478,
                username:"TalKing",
                comments: [],
            }
        ]
    },
    {
        id : 38,
        title:'What improved your quality of life so much, you wish you did it sooner?',
        content:"",
        creation:1637437777,
        lastUpdated:1657437738,
        username:"leeroyJenkins",
        comments: [],
    }
];


  const updated_posts = [
  {
      id : 42,
      title:'I am a giant cat that LOVES pineapple. AMA',
      content:"my cat loves pineapples.\nask a question about my cat, and I'll answer it",
      creation:"20/01/1970, 00:50:28",
      lastUpdated:"20/01/1970, 00:51:17",
      username:"TalKing",
      comments: [
          {
              id : 42,
              content:"ok...?",
              creation:"20/01/1970, 00:50:28",
              updated:"20/01/1970, 00:51:17",
              username:"TalKing",
              comments: [],
          }
      ]
  },
  {
      id : 38,
      title:'What improved your quality of life so much, you wish you did it sooner?',
      content:"",
      creation:"20/01/1970, 00:50:37",
      lastUpdated:"20/01/1970, 06:23:57",
      username:"leeroyJenkins",
      comments: [],
  }
]

    // test that reducer returns currect inital state without arguments
    it('reduce no argument', () => {
        expect(reducer()) .toStrictEqual({posts:[], comments:[]});
    });

  it('reduce ADD_POST', () => {
      expect(reducer(undefined                          , ADD_POST(posts[0])))  .toStrictEqual({posts:[updated_posts[0]], comments:[]});
      expect(reducer({posts:[], comments:[{id:42}]}     , ADD_POST(posts[1])))  .toStrictEqual({posts:[updated_posts[1]], comments:[{id:42}]});
  });

  it('reduce ADD_COMMENT', () => {     
    expect(reducer(undefined                        , ADD_COMMENT(posts[0].comments[0], 42)))  .toStrictEqual({posts:[]         , comments:[{comment:updated_posts[0].comments[0], postId:42}]});
    expect(reducer({posts:[{id:42}], comments:[]}   , ADD_COMMENT(posts[0].comments[0], 42)))  .toStrictEqual({posts:[{id:42}]  , comments:[{comment:updated_posts[0].comments[0], postId:42}]});
  });

  it('reduce RESET', () => {
    expect(reducer(undefined                                , RESET()))  .toStrictEqual({posts:[], comments:[]});
    expect(reducer({posts:[{id:37}], comments:[{id:42}]}    , RESET()))  .toStrictEqual({posts:[], comments:[]});
  });

});