import  actions from './todays-activities.types';


export const ADD_POST = post => ({
    type    : actions.ADD_POST,
    payload : post,
});

export const ADD_COMMENT = (comment, postId) => ({
    type    : actions.ADD_COMMENT,
    payload : {comment, postId},
});

export const RESET = () => ({
    type    : actions.RESET,
    payload : {},
});