const actions = {
    ADD_POST    : 'TODAYS_ACTIVITIES_ADD_POST',
    ADD_COMMENT : 'TODAYS_ACTIVITIES_ADD_COMMENT',
    RESET       : 'TODAYS_ACTIVITIES_RESET',
};

export default actions;