import actions from './todays-activities.types';
import { mapDateToReadableForComment, mapDateToReadableForPost} from '../global-utils/date.utils';

/** The inital state of the reducer, will return by callig reducer with no arguments */
const INITIAL_STATE = {posts:[], comments:[]};



/**
 * the post list reducer
 * @param {*} state the current state
 * @param {*} param1 the action to perform, that was recived from getPayload.
 * @returns the new state
 */
 const reducer = ( state=INITIAL_STATE, {type, payload} = {}) => {   
    switch (type) {

        case actions.ADD_POST:
            return {...state, posts: [...state.posts, mapDateToReadableForPost(payload) ]};

        case actions.ADD_COMMENT :
            let comment = payload.comment;
            let postId  = payload.postId;
            return {...state, comments: [...state.comments, {comment:mapDateToReadableForComment(comment), postId} ]};

        case actions.RESET :
            return INITIAL_STATE;
        

        default: 
            return state;
    }
  };



export default reducer;