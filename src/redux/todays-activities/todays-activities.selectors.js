import { createSelector } from 'reselect';


export const selectTodaysActivites = state => state.todays;

export const selectPosts = createSelector(
    [selectTodaysActivites],
    (activites) => activites.posts,
);

export const selectComments = createSelector(
    [selectTodaysActivites],
    (activites) => activites.comments,
);
