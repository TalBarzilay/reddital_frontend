import  actions from './todays-activities.types';
import {ADD_COMMENT, ADD_POST, RESET} from './todays-activities.actions';

//test getPayload function
describe('test todays-activites actions', () => {

    const posts = [
        {
            id : 42,
            title:'I am a giant cat that LOVES pineapple. AMA',
            content:"my cat loves pineapples.\nask a question about my cat, and I'll answer it",
            creation:1637428478,
            lastUpdated:1637477478,
            username:"TalKing",
            comments: [
                {
                    id : 42,
                    content:"with or without the shell?",
                    creation:1637428478,
                    lastUpdated:1637477478,
                    username:"TalKing",
                }
            ]
        },
    ];



    it('ADD_POST', () => {
      expect(ADD_POST(posts[0])).toStrictEqual({type: actions.ADD_POST , payload:posts[0]});
    });

    it('ADD_COMMENT', () => {
        expect(ADD_COMMENT(posts[0].comments[0], 33)).toStrictEqual({type: actions.ADD_COMMENT , payload:{comment:posts[0].comments[0], postId:33}});
    });

    it('RESET', () => {
        expect(RESET()).toStrictEqual({type: actions.RESET , payload:{}});
    });
    
});