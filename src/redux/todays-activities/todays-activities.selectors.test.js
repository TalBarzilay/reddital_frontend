import {selectTodaysActivites, selectPosts, selectComments} from './todays-activities.selectors';

//test getPayload function
describe('test todays-activites selectors', () => {

    const posts     = [{ id: 42}, { id: 43}, { id: 44}];
    const comments  = [{ id: 69}, { id: 70}, { id: 71}];

    const todays = {posts, comments};
    const state = {todays, banana:"banana"};


    it('selectTodaysActivites', () => {
        expect(selectTodaysActivites(state)).toBe(todays);
    });

    it('selectPosts', () => {
        expect(selectPosts(state)).toBe(posts);
    });

    it('selectComments', () => {
        expect(selectComments(state)).toBe(comments);
    });

  });