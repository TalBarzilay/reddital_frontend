import dateService from '../../services/date.service';

const datehandler = new dateService();


const toRedableDate = date => 
    typeof date === 'string' ? date : datehandler.date2string(datehandler.timestamp2date(date));

export const mapDateToReadableForComment = comment => {
    return {
        ...comment,
        creation: toRedableDate(comment.creation),
        updated : toRedableDate(comment.updated),
        comments: comment.comments == null ? [] : comment.comments.map(mapDateToReadableForComment)
    };
}

export const mapDateToReadableForPost = post => {
    return {
        ...post,
        creation: toRedableDate(post.creation),
        lastUpdated : toRedableDate(post.lastUpdated),
        comments: post.comments == null ? [] : post.comments.map(mapDateToReadableForComment)
    };
}