import {createStore, applyMiddleware} from 'redux';
import  logger  from 'redux-logger';

import rootReducer from './root-reducer';




// the middle wares to applay
const middlewares = [logger];

// the state store of our app
const store = createStore(rootReducer,  applyMiddleware(...middlewares ));


export default store;