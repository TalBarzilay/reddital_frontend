import {selectPostsOfSub, selectPosts, selectPost} from './posts-of-sub.selectors'

//test getPayload function
describe('test post-of-sub selectors', () => {

    const post = { id: 42};
    const posts = [post, { id: 43}, { id: 44}];
    const PostsOfSub = {posts};
    const state = {PostsOfSub, banana:"banana"};


    it('selectPostsOfSub', () => {
        expect(selectPostsOfSub(state)).toBe(PostsOfSub);
    });

    it('selectPosts', () => {
        expect(selectPosts(state)).toBe(posts);
    });

    it('selectPost', () => {
        expect(selectPost(state, 42)).toBe(post);
    });

  });