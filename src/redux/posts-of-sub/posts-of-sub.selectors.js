import { createSelector } from 'reselect';


export const selectPostsOfSub = state => state.PostsOfSub;

export const selectPosts = createSelector(
    [selectPostsOfSub],
    (PostsOfSub) => PostsOfSub.posts,
);

export const selectPost = (state, postid) => {
    const posts = selectPosts(state);
    return posts.find(post => post.id === postid);
}