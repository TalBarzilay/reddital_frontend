import actions from './posts-of-sub.types';

export const INIT_POSTS = posts =>
    ({type: actions.INIT_POSTS , payload:posts});

export const POST_POST = post =>
    ({type: actions.POST_POST , payload:post});

export const POST_COMMENT = (commentableid, comment) =>
    ({type: actions.POST_COMMENT , payload:{commentableid, comment}});

export const UPDATE = commentable =>
    ({type: actions.UPDATE , payload:commentable});