import  actions from './posts-of-sub.types';
import {INIT_POSTS, UPDATE, POST_COMMENT, POST_POST} from './posts-of-sub.actions'

//test getPayload function
describe('test post-of-sub actions', () => {

    const posts = [
        {
            id : 42,
            title:'I am a giant cat that LOVES pineapple. AMA',
            content:"my cat loves pineapples.\nask a question about my cat, and I'll answer it",
            creation:1637428478,
            lastUpdated:1637477478,
            username:"TalKing",
            comments: [
                {
                    id : 42,
                    content:"with or without the shell?",
                    creation:1637428478,
                    lastUpdated:1637477478,
                    username:"TalKing",
                }
            ]
        },
        {
            id : 38,
            title:'What improved your quality of life so much, you wish you did it sooner?',
            content:"",
            creation:1637437777,
            lastUpdated:1657437738,
            username:"leeroyJenkins"
        },
        {
            id : 69,
            title:'the first paragrapg of the wikipedia article about Frog. A frog is any member of a diverse and largely carnivorous group of short-bodied, tailless amphibians composing the order Anura.',
            content:"A frog is any member of a diverse and largely carnivorous group of short-bodied, tailless amphibians composing the order Anura (literally without tail in Ancient Greek). The oldest fossil proto-frog Triadobatrachus is known from\nthe Early Triassic of Madagascar, but molecular clock dating suggests their split from other amphibians may extend further back to the Permian, 265 million years ago. Frogs are widely distributed, ranging from the tropics to\nsubarctic regions, but the greatest concentration of species diversity is in tropical rainforest. Frogs account for around 88% of extant amphibian species. They are also one of the five most diverse vertebrate orders.\n Warty frog species tend to be called toads, but the distinction between frogs and toads is informal, not from taxonomy or evolutionary history.\nAn adult frog has a stout body, protruding eyes, anteriorly-attached tongue, limbs folded underneath, and no tail (the tail of tailed frogs is an extension of the male cloaca). Frogs have glandular skin, with secretions ranging from\ndistasteful to toxic. Their skin varies in colour from well-camouflaged dappled brown, grey and green to vivid patterns of bright red or yellow and black to show toxicity and ward off predators. Adult frogs live in fresh water and on dry land;\nsome species are adapted for living underground or in trees.",
            creation:1637538738,
            lastUpdated:1647437738,
            username:"TalKing"
        },
        {
            id : 420,
            title:'If the election to the USA would accure now, who do you think would have won, any why?',
            content:"I think spongebob Squarepants woud won, mostly because he is a sponge.",
            creation:1637477777,
            lastUpdated:2757437738,
            username:"leeroyJenkins"
        },
    ];



    it('INIT', () => {
      expect(INIT_POSTS(posts)).toStrictEqual({type: actions.INIT_POSTS , payload:posts});
    });

    it('UPDATE', () => {
        expect(UPDATE(posts[0])).toStrictEqual({type: actions.UPDATE , payload:posts[0]});
    });

    it('POST_POST', () => {
        expect(POST_POST(posts[0])).toStrictEqual({type: actions.POST_POST , payload:posts[0]});
    });

    it('POST_COMMENT', () => {
        expect(POST_COMMENT(42, posts[0].comments[0])).toStrictEqual({type: actions.POST_COMMENT , payload:{commentableid:42, comment:posts[0].comments[0]}});
    });

    
  });