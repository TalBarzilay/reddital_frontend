import actions from './posts-of-sub.types';
import { mapDateToReadableForComment, mapDateToReadableForPost} from '../global-utils/date.utils';


/** The inital state of the reducer, will return by callig reducer with no arguments */
const INITIAL_STATE = {posts:[]};


/**
 * create a copy of commentable, add a comment to a commetable with id commentid if exsist
 * @param {*} commentable the commetable object
 * @param {*} id the commentable id ti comet on
 * @param {*} commet the new comment
 * @returns a coppy of the commetabe, that may have a new comment
 */
 const copyAndAddComment = (commentable, commentableid, commet) => {
    let copy = {...commentable, comments: commentable.comments.map(com => copyAndAddComment(com, commentableid, commet))};

    if (commentable.id === commentableid){
        copy.comments = [...copy.comments, mapDateToReadableForComment(commet)];
    }

    return copy;
}

/**
 * create a copy of commentable, update a commentanle with id to the given one
 * @param {*} commentable the commetable object
 * @param {*} item the new values to use
 * @returns a coppy of the commetabe, that may have an updated commentabe comment
 */
const copyAndUpdate = (commentable, item) => {
    let copy = {...commentable, comments: commentable.comments.map(com => copyAndUpdate(com, item))};

    if (commentable.id === item.id){
        copy = {...copy, upVotes: item.upVotes, downVotes: item.downVotes, emotes: item.emotes, content:item.content};

        if(item.title){
            copy = {...copy, title:item.title};
        }
    }


    return copy;
}



/**
 * the post list reducer
 * @param {*} state the current state
 * @param {*} param1 the action to perform, that was recived from getPayload.
 * @returns the new state
 */
 const reducer = ( state=INITIAL_STATE, {type, payload} = {}) => {
    let commentableid;
    let commentable;
    
    switch (type) {

        case actions.INIT_POSTS:
            // update the dates from a timestamp to a date object
            return {...state, posts: payload.map(mapDateToReadableForPost)};

        case actions.POST_POST :
            return {...state, posts: [...state.posts, mapDateToReadableForPost(payload)]};

        case actions.POST_COMMENT :
            commentableid  = payload.commentableid;
            commentable = payload.comment;
            return {...state, posts: state.posts.map(post => copyAndAddComment(post, commentableid, commentable))};


        case actions.UPDATE:
            return {...state, posts: state.posts.map(post => copyAndUpdate(post, payload))};
        

        default: 
            return state;
    }
  };



export default reducer;