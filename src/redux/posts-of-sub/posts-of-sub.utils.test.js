import {getComment} from './posts-of-sub.utils';


describe('test post-of-sub utils', () => {

    const commentable = {
        id:42,
        comments:[{
            id:43,
            comments:[{
                id:44,
                comments:[{
                    id:45,
                    comments:[{
                        id:46,
                        comments:[{
                            id:47,
                            comments:[]
                        }]
                    }]
                }]
            }]
        }]
    };


    it('get comment', () => {
        expect(getComment(commentable, 43)).toBeDefined();
        expect(getComment(commentable, 44)).toBeDefined();
        expect(getComment(commentable, 45)).toBeDefined();
        expect(getComment(commentable, 46)).toBeDefined();
        expect(getComment(commentable, 47)).toBeDefined();
        
        expect(getComment(commentable, 48)).toBeUndefined();
    });

  });