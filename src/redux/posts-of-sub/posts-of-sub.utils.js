export const getComment = (commentble, commentid) => {
    const temp = commentble.comments.find(comment => comment.id === commentid);

    return temp !== undefined ? 
        temp :
        commentble.comments.map(comment => getComment(comment, commentid)).find(comment => comment !== undefined);
}