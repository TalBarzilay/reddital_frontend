import reducer from './posts-of-sub.reducer';
import {INIT_POSTS, UPDATE, POST_POST, POST_COMMENT} from './posts-of-sub.actions';

process.env.NODE_ENV = 'test';

// test reducer function
describe('posts-of-sub reducer', () => {

    const posts = [
        {
            id : 42,
            title:'I am a giant cat that LOVES pineapple. AMA',
            content:"my cat loves pineapples.\nask a question about my cat, and I'll answer it",
            creation:1637428478,
            lastUpdated:1637477478,
            username:"TalKing",
            comments: [
                {
                    id : 42,
                    content:"ok...?",
                    creation:1637428478,
                    updated:1637477478,
                    username:"TalKing",
                    comments: [],
                }
            ]
        },
        {
            id : 38,
            title:'What improved your quality of life so much, you wish you did it sooner?',
            content:"",
            creation:1637437777,
            lastUpdated:1657437738,
            username:"leeroyJenkins",
            comments: [],
        }
    ];


    const updated_posts = [
      {
          id : 42,
          title:'I am a giant cat that LOVES pineapple. AMA',
          content:"my cat loves pineapples.\nask a question about my cat, and I'll answer it",
          creation:"20/01/1970, 00:50:28",
          lastUpdated:"20/01/1970, 00:51:17",
          username:"TalKing",
          comments: [
              {
                  id : 42,
                  content:"ok...?",
                  creation:"20/01/1970, 00:50:28",
                  updated:"20/01/1970, 00:51:17",
                  username:"TalKing",
                  comments: [],
              }
          ]
      },
      {
          id : 38,
          title:'What improved your quality of life so much, you wish you did it sooner?',
          content:"",
          creation:"20/01/1970, 00:50:37",
          lastUpdated:"20/01/1970, 06:23:57",
          username:"leeroyJenkins",
          comments: [],
      }
  ]

    // test that reducer returns currect inital state without arguments
    it('reduce no argument', () => {
        expect(reducer()) .toStrictEqual({posts:[]});
    });

    //test reducer's INIT_POSTS action
    it('reduce INIT_POSTS', () => {
      expect(reducer(undefined  , INIT_POSTS(posts)))  .toStrictEqual({posts:updated_posts});
      expect(reducer({posts:[]} , INIT_POSTS(posts)))  .toStrictEqual({posts:updated_posts});
  });

  it('reduce UPDATE', () => {     
    expect(reducer({posts:[{id:42, upVotes:0, downVotes:0, title:"", content:"", emotes:[], comments:[]}]}             , UPDATE({id:42, upVotes:1, emotes:["upvote"],  title:"t", content:"c", downVotes:0})))                                     .toStrictEqual({posts:[{id:42, upVotes:1, downVotes:0, emotes:["upvote"],  title:"t", content:"c", comments:[]}]});
    expect(reducer({posts:[{id:37, upVotes:42, downVotes:39, content:"", emotes:[], comments:[{id:42, content:"",  comments:[]}]}]}    , UPDATE({id:37, content:"hello!", upVotes:88, downVotes:69, emotes:["banana", "marshemellow"]}))) .toStrictEqual({posts:[{id:37, upVotes:88, downVotes:69, emotes:["banana", "marshemellow"], content:"hello!",  comments:[{id:42, content:"", comments:[]}]}]});
    
    expect(reducer({posts:[{id:42, upVotes:1, downVotes:0, content:"", emotes:["upvote"], comments:[{id:69, upVotes:0, downVotes:0, content:"", emotes:[], comments:[]}]}]}             , UPDATE({id:69, upVotes:77, downVotes:98, content:"new", emotes:["banana"], comments:[]})))      .toStrictEqual({posts:[{id:42, upVotes:1, downVotes:0, emotes:["upvote"], content:"", comments:[{id:69, upVotes:77, downVotes:98, content:"new", emotes:["banana"], comments:[]}]}]}); 
    expect(reducer({posts:[{id:42, upVotes:1, downVotes:0, content:"", emotes:["upvote"], comments:[{id: 101, content:"", comments:[{id:11, content:"", comments:[]}, {id:420, content:"", comments:[{id:69, upVotes:0, content:"", downVotes:0, emotes:[], comments:[]}]}]}]}]}             , UPDATE({id:69, upVotes:77, downVotes:98, content:"", emotes:["banana"], comments:[]})))      .toStrictEqual({posts:[{id:42, content:"", upVotes:1, downVotes:0, emotes:["upvote"], comments:[{id: 101, content:"", comments:[{id:11, content:"", comments:[]}, {id:420, content:"", comments:[{id:69, content:"", upVotes:77, downVotes:98, emotes:["banana"], comments:[]}]}]}]}]});
  }); 


  it('reduce POST_POST', () => {     
    expect(reducer({posts:[{id:42, upVotes:0, downVotes:0, emotes:[], comments:[]}]}             , POST_POST({id:99, creation:1637428478,lastUpdated:1637477478, upVotes:1, emotes:["upvote"], downVotes:0, comments:[]}))).toStrictEqual({posts:[{id:42, upVotes:0, downVotes:0, emotes:[], comments:[]},{id:99, upVotes:1, emotes:["upvote"],creation:"20/01/1970, 00:50:28", lastUpdated:"20/01/1970, 00:51:17", downVotes:0, comments:[]}]});
  }); 

  it('reduce POST_COMMENT', () => {     
    expect(reducer({posts:[{id:77, upVotes:0, downVotes:0, emotes:[], comments:[{id:99, upVotes:0, downVotes:0, emotes:[], comments:[{id:11, upVotes:0, downVotes:0, emotes:[], comments:[]}, {id:555, upVotes:0, downVotes:0, emotes:[], comments:[{id:42, upVotes:0, downVotes:0, emotes:[], comments:[]}]}]}]}]}             , POST_COMMENT(42, {id:111, creation:1637428478,updated:1637477478, upVotes:1, emotes:["upvote"], downVotes:0, comments:[]}))).toStrictEqual({posts:[{id:77, upVotes:0, downVotes:0, emotes:[], comments:[{id:99, upVotes:0, downVotes:0, emotes:[], comments:[{id:11, upVotes:0, downVotes:0, emotes:[], comments:[]}, {id:555, upVotes:0, downVotes:0, emotes:[], comments:[{id:42, upVotes:0, downVotes:0, emotes:[], comments:[{id:111, upVotes:1, emotes:["upvote"],creation:"20/01/1970, 00:50:28", updated:"20/01/1970, 00:51:17", downVotes:0, comments:[]}]}]}]}]}]}  );
  }); 

});