const actions = {
    INIT_POSTS              : 'POST_LIST_INIT',
    POST_POST               : 'POST_POST',
    POST_COMMENT            : 'POST_COMMENT',
    UPDATE                  : 'COMENT_POST_VIEW_UPDATE',
};

export default actions;