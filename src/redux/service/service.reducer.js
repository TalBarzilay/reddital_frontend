import dateService      from '../../services/date.service';
import categoryService  from '../../services/category.service';
import messageService   from '../../services/message.service';
import postService      from '../../services/post.service';
import userService      from '../../services/user.service';
import authenticationService from '../../services/authentication.service';
import biService from '../../services/bi.service';


/** The inital state of the reducer, will return by callig reducer with no arguments */
const INITIAL_STATE = {
    dateService           : new dateService(),
    categoryService       : new categoryService(),
    messageService        : new messageService(),
    postService           : new postService(),
    userService           : new userService(),
    biService             : new biService(),
    authenticationService : authenticationService.getInstance(),
};


/**
 * the reducer
 * @param {*} state the current state
 * @param {*} param1 the action to perform, that was recived from getPayload.
 * @returns the new state
 */
 const reducer = ( state=INITIAL_STATE, {type, payload} = {}) => {
    
    switch (type) {
        default: 
            return state;
    }
  };



export default reducer;