import reducer from './service.reducer';

describe('test services reducer redux', () => {

    it('reduce no argument', () => {
        const state = reducer();

        expect(state.dateService)           .toBeDefined();
        expect(state.categoryService)       .toBeDefined();
        expect(state.messageService)        .toBeDefined();
        expect(state.postService)           .toBeDefined();
        expect(state.userService)           .toBeDefined();
        expect(state.authenticationService) .toBeDefined();
        expect(state.biService) .toBeDefined();
        
        expect(state.banana)                .toBeUndefined();
    });

  });