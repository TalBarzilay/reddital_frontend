import {selectServices, selectDateService, selectCategoryService, selectMessageService, selectPostService, selectUserService, selectAuthenticationService, selectBiService} from './service.selectors';

describe('test services selectors redux', () => {

    const dateService           = "dateService";
    const categoryService       = "categoryService";
    const messageService        = "messageService" ;
    const postService           = "postService";
    const userService           = "postService";
    const authenticationService = "authenticationService";
    const biService             = "biService";
    
    const services = {
        dateService,
        categoryService,
        messageService,
        postService,
        userService,
        authenticationService,
        biService,
    }

    const state = {services, banana:"banana"};

    it('selectServices', () => {
        expect(selectServices(state)).toStrictEqual(services);
    });


    it('selectDateService', () => {
        expect(selectDateService(state)).toStrictEqual(dateService);
    });

    it('selectCategoryService', () => {
        expect(selectCategoryService(state)).toStrictEqual(categoryService);
    });

    it('selectMessageService', () => {
        expect(selectMessageService(state)).toStrictEqual(messageService);
    });

    it('selectPostService', () => {
        expect(selectPostService(state)).toStrictEqual(postService);
    });

    it('selectUserService', () => {
        expect(selectUserService(state)).toStrictEqual(userService);
    });

    it('selectAuthenticationService', () => {
        expect(selectAuthenticationService(state)).toStrictEqual(authenticationService);
    });

    it('selectBiService', () => {
        expect(selectBiService(state)).toStrictEqual(biService);
    });


  });