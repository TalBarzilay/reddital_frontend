import { createSelector } from 'reselect';


export const selectServices = state => state.services;

export const selectDateService = createSelector(
    [selectServices],
    (services) => services.dateService,
);

export const selectCategoryService = createSelector(
    [selectServices],
    (services) => services.categoryService,
);

export const selectMessageService = createSelector(
    [selectServices],
    (services) => services.messageService,
);

export const selectPostService = createSelector(
    [selectServices],
    (services) => services.postService,
);

export const selectUserService = createSelector(
    [selectServices],
    (services) => services.userService,
);

export const selectAuthenticationService = createSelector(
    [selectServices],
    (services) => services.authenticationService,
);

export const selectBiService = createSelector(
    [selectServices],
    (services) => services.biService,
);