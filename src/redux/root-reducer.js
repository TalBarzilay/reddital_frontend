import {combineReducers} from 'redux';

import PostsOfSubReducer from './posts-of-sub/posts-of-sub.reducer';
import serviceReducer from './service/service.reducer';
import todaysReducer from './todays-activities/todays-activities.reducer';


const rootReducer = combineReducers({
    PostsOfSub  : PostsOfSubReducer,
    services    : serviceReducer,
    todays      : todaysReducer,
});


export default rootReducer;